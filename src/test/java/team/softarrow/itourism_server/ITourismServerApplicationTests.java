package team.softarrow.itourism_server;

import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = ITourismServerApplication.class)
public class ITourismServerApplicationTests {

}
