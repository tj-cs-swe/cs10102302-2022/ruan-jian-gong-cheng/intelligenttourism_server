package team.softarrow.itourism_server.service;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.transaction.annotation.Transactional;
import team.softarrow.itourism_server.ITourismServerApplication;
import team.softarrow.itourism_server.entity.ServicePoint;
import team.softarrow.itourism_server.exception.DefinedException;
import team.softarrow.itourism_server.security.VisitorDetail;

@SpringBootTest(classes = ITourismServerApplication.class)
public class ServicePointServiceTest {
    @Autowired
    private ServicePointService servicePointService;

    @BeforeEach
    public void init() {
        UsernamePasswordAuthenticationToken authRequest;
        VisitorDetail visitorDetail = new VisitorDetail();
        visitorDetail.setId(1);
        visitorDetail.setUsername("yzh");
        visitorDetail.setAuthoritiesFromString("ROLE_spot_admin");
        visitorDetail.setSpotId(1);
        authRequest = new UsernamePasswordAuthenticationToken(visitorDetail, "123");
        SecurityContextHolder.getContext().setAuthentication(authRequest);
    }

    @Test
    public void getPointTest() {
        ServicePoint servicePoint = servicePointService.getServicePointByID(1);
        Assertions.assertEquals(Integer.valueOf(1), servicePoint.getId());
    }

    @Test
    @Transactional
    public void getListTest() {
        servicePointService.getServicePointList(1, 0, 5);
    }

    @Test
    @Transactional
    public void addPointTest() {
        ServicePoint servicePoint = new ServicePoint();
        servicePoint.setName("测试测试");
        servicePoint.setIntroduce("介绍介绍");
        servicePoint.setPosition("10,20");
        servicePoint.setFigure("图片图片");

        Throwable exception = Assertions.assertThrows(DefinedException.class, () -> {
            servicePointService.addServicePoint(servicePoint);
        });
        Assertions.assertEquals("类型为空",exception.getMessage());

        servicePoint.setType("scene");
        servicePointService.addServicePoint(servicePoint);
    }

    @Test
    @Transactional
    public void editPointTest() {
        ServicePoint servicePoint = new ServicePoint();
        servicePoint.setId(1);
        servicePoint.setType("toilet");
        servicePoint.setPosition("1,2");
        servicePointService.editServicePoint(servicePoint);
    }

    @Test
    @Transactional
    public void deletePointTest() {
        servicePointService.deleteServicePoint(1);
    }
}
