package team.softarrow.itourism_server.service;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.annotation.Rollback;
import org.springframework.transaction.annotation.Transactional;
import team.softarrow.itourism_server.ITourismServerApplication;
import team.softarrow.itourism_server.entity.Permission;
import team.softarrow.itourism_server.entity.User;
import team.softarrow.itourism_server.exception.DefinedException;
import team.softarrow.itourism_server.security.VisitorDetail;

import java.util.HashMap;
import java.util.Map;

@SpringBootTest(classes = ITourismServerApplication.class)
public class UserServiceTest {

    @Autowired
    private UserService userService;

    @BeforeEach
    public void init() {
        UsernamePasswordAuthenticationToken authRequest;
        VisitorDetail visitorDetail = new VisitorDetail();
        visitorDetail.setId(1);
        visitorDetail.setUsername("yzh");
        authRequest = new UsernamePasswordAuthenticationToken(visitorDetail, "123");
        SecurityContextHolder.getContext().setAuthentication(authRequest);
    }

    @Test
    public void findByIdTest() {
        User user = userService.getUserById(1);
        Assertions.assertEquals(Integer.valueOf(1), user.getId());
    }

    @Test
    public void currentTest() {
        int id = userService.getCurrent().getId();
        Assertions.assertEquals(1, id);
    }

    @Test
    @Transactional
    public void userListTest() {
        userService.getUserList(0, 5);
    }

    @Test
    @Transactional
    @Rollback
    public void permissionChangeTest() {
        Map<String, Integer> info = new HashMap<>();
        info.put("users_id", 1);
        info.put("permissions_id", 11);
        userService.changeUserPermission(info);

        User user = userService.getUserById(1);
        Permission permission = new Permission();
        permission.setId(11);
        Assertions.assertTrue(user.getPermissions().contains(permission));
    }

    @Test
    public void getTicketOrderTest() {
        Assertions.assertNotNull(userService.getTicketOrderBelongMe(0, 5));
    }

    @Test
    public void getCommodityOrderTest() {
        Assertions.assertNotNull(userService.getCommodityOrderBelongMe(0, 5));
    }

    @Test
    @Transactional
    @Rollback
    public void editUserTest() {
        Map<String, String> info = new HashMap<>();
        info.put("age", "50");
        info.put("gender", "假");
        Assertions.assertThrows(
                DefinedException.class, () -> userService.edit(info));
        info.put("age", "500");
        info.put("gender", "女");
        Assertions.assertThrows(
                DefinedException.class, () -> userService.edit(info));
        info.put("age", "50");
        info.put("gender", "保密");
        userService.edit(info);
        User user = userService.getUserById(1);
        Assertions.assertEquals(Integer.valueOf(50), user.getAge());
        Assertions.assertEquals("保密", user.getGender());
    }

    @Test
    public void ticketsTest() {
        Assertions.assertNotNull(userService.getTicketBelongMe(0, 5));
    }

    @Test
    @Transactional
    @Rollback
    public void avatarTest() {
        userService.uploadAvatar("my_test");
        User user = userService.getCurrentUserInfo();
        Assertions.assertEquals("my_test", user.getAvatar());
    }

}
