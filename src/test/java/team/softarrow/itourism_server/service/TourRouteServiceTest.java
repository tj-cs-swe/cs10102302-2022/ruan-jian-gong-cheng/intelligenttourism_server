package team.softarrow.itourism_server.service;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.annotation.Rollback;
import org.springframework.transaction.annotation.Transactional;
import team.softarrow.itourism_server.ITourismServerApplication;
import team.softarrow.itourism_server.entity.TourRoute;
import team.softarrow.itourism_server.exception.DefinedException;
import team.softarrow.itourism_server.security.VisitorDetail;

@SpringBootTest(classes = ITourismServerApplication.class)
public class TourRouteServiceTest {
    @Autowired
    private TourRouteService tourRouteService;

    @BeforeEach
    public void init() {
        UsernamePasswordAuthenticationToken authRequest;
        VisitorDetail visitorDetail = new VisitorDetail();
        visitorDetail.setId(1);
        visitorDetail.setUsername("yzh");
        visitorDetail.setAuthoritiesFromString("ROLE_spot_admin");
        visitorDetail.setSpotId(1);
        authRequest = new UsernamePasswordAuthenticationToken(visitorDetail, "123");
        SecurityContextHolder.getContext().setAuthentication(authRequest);
    }

    @Test
    @Transactional
    @Rollback
    public void addRouteTest() {
        TourRoute tourRoute = new TourRoute();
        tourRoute.setName("测试增加");
        tourRoute.setIntroduce("测试增加介绍");
        Throwable exception = Assertions.assertThrows(DefinedException.class, () -> {
            tourRouteService.addTourRoute(tourRoute);
        });
        Assertions.assertEquals("浏览路线内容为空",exception.getMessage());

        tourRoute.setRoute("1;2");
        tourRouteService.addTourRoute(tourRoute);
    }

    @Test
    @Transactional
    @Rollback
    public void editTourRouteTest() {
        TourRoute tourRoute = new TourRoute();
        tourRoute.setId(3);
        tourRoute.setRoute("3;4");
        tourRouteService.editTourRoute(tourRoute);
    }
}
