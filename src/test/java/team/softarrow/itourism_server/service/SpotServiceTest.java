package team.softarrow.itourism_server.service;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.transaction.annotation.Transactional;
import team.softarrow.itourism_server.ITourismServerApplication;
import team.softarrow.itourism_server.entity.Shop;
import team.softarrow.itourism_server.entity.Spot;
import team.softarrow.itourism_server.exception.DefinedException;
import team.softarrow.itourism_server.security.VisitorDetail;

import java.util.List;

@SpringBootTest(classes = ITourismServerApplication.class)
public class SpotServiceTest {
    @Autowired
    private SpotService spotService;

    @BeforeEach
    public void init() {
        UsernamePasswordAuthenticationToken authRequest;
        VisitorDetail visitorDetail = new VisitorDetail();
        visitorDetail.setId(1);
        visitorDetail.setUsername("yzh");
        visitorDetail.setAuthoritiesFromString("ROLE_admin");
        authRequest = new UsernamePasswordAuthenticationToken(visitorDetail, "123");
        SecurityContextHolder.getContext().setAuthentication(authRequest);
    }

    @Test
    public void getSpotListTest() {
        spotService.getSpotList(0, 5);
    }

    @Test
    public void getSpotTest() {
        Spot spot = spotService.getSpot(1);
        Assertions.assertEquals( Integer.valueOf(1), spot.getId());
    }

    @Test
    @Transactional
    public void addSpotTest() {
        Spot spot = new Spot();
        spot.setName("测试");
        spot.setIntroduction("测试测试");
        spot.setLeftTickets(100L);
        spot.setTicketPrice(10.0);
        Throwable exception = Assertions.assertThrows(DefinedException.class, () -> {
            spotService.addSpot(spot);
        });
        Assertions.assertEquals("图片为空",exception.getMessage());

        spot.setFigure("daw");
        spot.setPosition("1,1");
        spotService.addSpot(spot);
    }

    @Test
    @Transactional
    public void editSpotTest() {
        Spot spot = new Spot();
        spot.setId(1);
        spot.setIntroduction("测试测试改改");
        spotService.editSpot(spot);
    }

    @Test
    @Transactional
    public void deleteSpotTest() {
        spotService.deleteSpot(1);
    }

    @Test
    @Transactional
    public void getShopInSpotTest() {
        List<Shop> shops = spotService.getShopsInSpot(1, 0, 5);
        for (Shop shop : shops) {
            System.out.println(shop.getId());
        }
    }
}
