package team.softarrow.itourism_server.service;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.transaction.annotation.Transactional;
import team.softarrow.itourism_server.ITourismServerApplication;
import team.softarrow.itourism_server.entity.Commodity;
import team.softarrow.itourism_server.entity.Order;
import team.softarrow.itourism_server.entity.Shop;
import team.softarrow.itourism_server.exception.DefinedException;
import team.softarrow.itourism_server.security.VisitorDetail;

import java.util.List;

@SpringBootTest(classes = ITourismServerApplication.class)
public class SellerServiceTest {
    @Autowired
    private SellerService sellerService;

    @BeforeEach
    public void init() {
        UsernamePasswordAuthenticationToken authRequest;
        VisitorDetail visitorDetail = new VisitorDetail();
        visitorDetail.setId(1);
        visitorDetail.setUsername("yzh");
        visitorDetail.setAuthoritiesFromString("ROLE_seller");
        visitorDetail.setShopId(1);
        authRequest = new UsernamePasswordAuthenticationToken(visitorDetail, "123");
        SecurityContextHolder.getContext().setAuthentication(authRequest);
    }

    @Test
    @Transactional
    public void editShopTest() {
        Shop shop = new Shop();
        shop.setId(2);
        shop.setIntroduction("测试测试");
        Throwable exception = Assertions.assertThrows(DefinedException.class, () -> {
            sellerService.editShop(shop);
        });
        Assertions.assertEquals("获取用户认证失败",exception.getMessage());

        shop.setId(1);
        sellerService.editShop(shop);
    }

    @Test
    @Transactional
    public void addCommodityTest() {
        Commodity commodity = new Commodity();
        commodity.setName("名字");
        commodity.setIntroduction("介绍");
        commodity.setFigure("图片");
        Throwable exception = Assertions.assertThrows(DefinedException.class, () -> {
            sellerService.addCommodity(commodity);
        });
        Assertions.assertEquals("价格为空",exception.getMessage());

        commodity.setPrice(10.0);
        sellerService.addCommodity(commodity);
    }

    @Test
    @Transactional
    public void editCommodityTest() {
        Commodity commodity = new Commodity();
        commodity.setId(2);
        commodity.setName("测试");

        Throwable exception = Assertions.assertThrows(DefinedException.class, () -> {
            sellerService.editCommodity(commodity);
        });
        Assertions.assertEquals("获取用户认证失败",exception.getMessage());

        commodity.setId(12);
        sellerService.editCommodity(commodity);
    }

    @Test
    @Transactional
    public void deleteCommodityTest() {
        sellerService.deleteCommodity(12);
    }

    @Test
    public void getOrdersInShopTest() {
        List<Order> orderList = sellerService.getOrdersInShop(0, 5);
        for (Order order : orderList) {
            Assertions.assertNotNull(order.getCommodity());
        }
    }
}
