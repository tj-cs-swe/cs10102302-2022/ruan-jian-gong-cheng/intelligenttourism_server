package team.softarrow.itourism_server.service;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.annotation.Rollback;
import org.springframework.transaction.annotation.Transactional;
import team.softarrow.itourism_server.ITourismServerApplication;
import team.softarrow.itourism_server.entity.Comment;
import team.softarrow.itourism_server.entity.ServicePoint;
import team.softarrow.itourism_server.exception.DefinedException;
import team.softarrow.itourism_server.security.VisitorDetail;

@SpringBootTest(classes = ITourismServerApplication.class)
public class CommentServiceTest {
    @Autowired
    private CommentService commentService;

    @BeforeEach
    public void init() {
        UsernamePasswordAuthenticationToken authRequest;
        VisitorDetail visitorDetail = new VisitorDetail();
        visitorDetail.setId(1);
        visitorDetail.setUsername("yzh");
        visitorDetail.setAuthoritiesFromString("ROLE_spot_admin");
        visitorDetail.setSpotId(1);
        authRequest = new UsernamePasswordAuthenticationToken(visitorDetail, "123");
        SecurityContextHolder.getContext().setAuthentication(authRequest);
    }

    @Test
    public void findByIdTest() {
        Comment comment = commentService.getCommentById(1);
        Assertions.assertEquals(Integer.valueOf(1), comment.getId());
    }

    @Test
    @Transactional
    @Rollback
    public void addCommentTest() {
        Comment comment = new Comment();
        ServicePoint servicePoint = new ServicePoint();
        servicePoint.setId(1);
        comment.setServicePoint(servicePoint);
        Throwable exception = Assertions.assertThrows(DefinedException.class, () -> {
            commentService.addComment(comment);
        });
        Assertions.assertEquals("评论内容为空",exception.getMessage());

        comment.setContain("test test test");
        commentService.addComment(comment);
    }

    @Test
    @Transactional
    @Rollback
    public void deleteCommentTest() {
        commentService.deleteComment(1);
    }

    @Test
    @Transactional
    @Rollback
    public void likeCommentTest() {
        Comment comment = commentService.getCommentById(1);
        commentService.commentUnDoLike(1);
        Integer old_like = comment.getLikeNum();
        commentService.commentLike(1);
        comment = commentService.getCommentById(1);
        Integer plus = comment.getLikeNum() - old_like;
        Assertions.assertEquals(Integer.valueOf(1), plus);

        commentService.commentUnDoLike(1);
        comment = commentService.getCommentById(1);
        plus = comment.getLikeNum() - old_like;
        Assertions.assertEquals(Integer.valueOf(0), plus);
    }

    @Test
    public void getCommentListTest() {
        Assertions.assertNotNull(commentService.getCommentInServicePoint(1, 0, 5));
    }

    @Test
    @Transactional
    @Rollback()
    public void topAndVerifyTest() {
        commentService.topComment(1);
        commentService.verifyComment(1);
    }
}
