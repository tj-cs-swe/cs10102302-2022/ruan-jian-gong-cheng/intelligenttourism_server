package team.softarrow.itourism_server.service;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.annotation.Rollback;
import org.springframework.transaction.annotation.Transactional;
import team.softarrow.itourism_server.ITourismServerApplication;
import team.softarrow.itourism_server.entity.IntroAudio;
import team.softarrow.itourism_server.entity.ServicePoint;
import team.softarrow.itourism_server.exception.DefinedException;
import team.softarrow.itourism_server.security.VisitorDetail;

@SpringBootTest(classes = ITourismServerApplication.class)
public class AudioServiceTest {
    @Autowired
    private AudioService audioService;

    @BeforeEach
    public void init() {
        UsernamePasswordAuthenticationToken authRequest;
        VisitorDetail visitorDetail = new VisitorDetail();
        visitorDetail.setId(1);
        visitorDetail.setUsername("yzh");
        visitorDetail.setAuthoritiesFromString("ROLE_spot_admin");
        visitorDetail.setSpotId(1);
        authRequest = new UsernamePasswordAuthenticationToken(visitorDetail, "123");
        SecurityContextHolder.getContext().setAuthentication(authRequest);
    }

    @Test
    @Transactional
    @Rollback
    public void addAudioTest() {
        IntroAudio introAudio = new IntroAudio();
        introAudio.setIntroduce("测试");
        introAudio.setName("介绍xxx");
        introAudio.setUrl("no_file.mp3");
        ServicePoint servicePoint = new ServicePoint();
        servicePoint.setId(1);
        introAudio.setServicePoint(servicePoint);
        Throwable exception = Assertions.assertThrows(DefinedException.class, () -> {
            audioService.addIntroAudio(introAudio);
        });
        Assertions.assertEquals("尚未上传音频文件",exception.getMessage());
    }

    @Test
    public void getAudioListTest() {
        Assertions.assertNotNull(audioService.getIntroAudioInServicePoint(1, 0, 5));
    }

    @Test
    public void getAudioTest() {
        IntroAudio introAudio = audioService.getAudioById(1);
        Assertions.assertEquals(Integer.valueOf(1), introAudio.getId());

        Throwable exception = Assertions.assertThrows(DefinedException.class, () -> {
            audioService.getAudioById(999999);
        });
        Assertions.assertEquals("找不到音频",exception.getMessage());
    }

    @Test
    @Transactional
    @Rollback
    public void editAudioTest() {
        IntroAudio introAudio = new IntroAudio();
        introAudio.setId(1);
        introAudio.setName("改改");
        introAudio.setIntroduce("改改介绍");
        audioService.editIntroAudio(introAudio);
    }
}
