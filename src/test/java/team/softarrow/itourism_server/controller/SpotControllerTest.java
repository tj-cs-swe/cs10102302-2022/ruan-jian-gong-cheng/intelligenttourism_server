package team.softarrow.itourism_server.controller;

import com.alibaba.fastjson.JSONObject;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;
import team.softarrow.itourism_server.entity.Spot;

/**
 * 景区管理测试
 */
@Transactional
@RunWith(SpringRunner.class)
@SpringBootTest
public class SpotControllerTest {
    @Autowired
    SpotController spotController;

    // 初始化
    @Before
    public void setUp() {
        System.out.println("执行初始化");
    }

    // 测试
//    @Test
//    public void testAdd() throws Exception {
//        Spot spot = new Spot();
//        spot.setId(1);
//        spot.setName("大光明神殿");
//        spot.setIntroduction("光明神王故居，先用做香火供奉");
//        Spot retSpot = spotController.addSpot(spot);
//        System.out.println(retSpot);
//    }
    @Test
    public void testFind() {
        Spot retSpot = spotController.find("五台山");
        System.out.println(retSpot.getIntroduction());
    }
}
