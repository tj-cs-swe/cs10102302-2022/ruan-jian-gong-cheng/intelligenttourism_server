package team.softarrow.itourism_server.controller;

import com.alibaba.fastjson.JSON;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.transaction.annotation.Transactional;
import team.softarrow.itourism_server.ITourismServerApplicationTests;
import team.softarrow.itourism_server.response.ReturnBody;

import java.nio.charset.StandardCharsets;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class AuthControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Before
    public void setUp() {
        System.out.println("---------------start---------------");
    }

    @After
    public void end() {
        System.out.println("----------------end----------------");
    }

    @Test
    public void getCaptcha() throws Exception{
        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders
                .get("/auth/captcha")
        ).andExpect(MockMvcResultMatchers.status().isOk()).andReturn();
    }

    @Test
    public void login() throws Exception{
        String json = "{\n" +
                "    \"username\":\"loise\",\n" +
                "    \"password\":\"1234\",\n" +
                "    \"key\":\"5389fa25-96cf-4eec-a46f-277aa8e41b12\",\n" +
                "    \"captcha\":\"test\"\n" +
                "}\n";
        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders
                .post("/auth/login")
                .content(json.getBytes(StandardCharsets.UTF_8))
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .header("Authorization","")
        ).andExpect(MockMvcResultMatchers.status().isOk()).andReturn();
        String content = mvcResult.getResponse().getContentAsString();
        ReturnBody returnBody = JSON.parseObject(content, ReturnBody.class);
        Assert.assertEquals(returnBody.getStatus().intValue(), 402);
        System.out.println("通过");
    }

    @Test
    public void email() throws Exception{
        String json="{\n" +
                "    \"email\":\"617645779@qq.com\"\n" +
                "}\n";
        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders
                .post("/auth/email")
                .content(json.getBytes(StandardCharsets.UTF_8))
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .header("Authorization","")
        ).andExpect(MockMvcResultMatchers.status().isOk()).andReturn();
        System.out.println("通过");
    }

}


