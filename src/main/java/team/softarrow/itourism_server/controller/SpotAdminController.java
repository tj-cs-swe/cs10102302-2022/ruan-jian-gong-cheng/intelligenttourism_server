package team.softarrow.itourism_server.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import team.softarrow.itourism_server.entity.*;
import team.softarrow.itourism_server.service.*;

import java.util.Map;

@RestController
@RequestMapping("/spot_admin")
public class SpotAdminController {

    private SpotService spotService;
    private SellerService sellerService;
    private ServicePointService servicePointService;
    private CommentService commentService;
    private AudioService audioService;
    private UploadService uploadService;
    private TourRouteService tourRouteService;

    @Autowired
    public void setTourRouteService(TourRouteService tourRouteService) {
        this.tourRouteService = tourRouteService;
    }

    @Autowired
    public void setUploadService(UploadService uploadService) {
        this.uploadService = uploadService;
    }

    @Autowired
    public void setAudioService(AudioService audioService) {
        this.audioService = audioService;
    }

    @Autowired
    public void setCommentService(CommentService commentService) {
        this.commentService = commentService;
    }

    @Autowired
    public void setSellerService(SellerService sellerService) {
        this.sellerService = sellerService;
    }

    @Autowired
    public void setSpotService(SpotService spotService) {
        this.spotService = spotService;
    }

    @Autowired
    public void setServicePointService(ServicePointService servicePointService) {
        this.servicePointService = servicePointService;
    }

    /**
     * 修改景区信息
     */
    @PostMapping("/spot/edit")
    public boolean edit(@RequestBody Spot spot){
        spotService.editSpot(spot);
        return true;
    }

    @PostMapping("/service_point/add")
    public boolean addServicePoint(@RequestBody ServicePoint shop) {
        servicePointService.addServicePoint(shop);
        return true;
    }

    @PostMapping("/service_point/edit")
    public boolean editServicePoint(@RequestBody ServicePoint shop) {
        servicePointService.editServicePoint(shop);
        return true;
    }

    @DeleteMapping("/service_point/delete/{id}")
    public boolean deleteServicePoint(@PathVariable Integer id) {
        servicePointService.deleteServicePoint(id);
        return true;
    }

    @DeleteMapping("/comment/delete/{id}")
    public boolean deleteComment(@PathVariable Integer id) {
        commentService.deleteComment(id);
        return true;
    }

    @PostMapping("/comment/up/{id}")
    public boolean topComment(@PathVariable Integer id) {
        commentService.topComment(id);
        return true;
    }

    @PostMapping("/upload")
    public Map<String, String> uploadAudio(@RequestPart(value = "file") MultipartFile file) {
        return Map.of("path", uploadService.uploadFile(file));
    }

    @PostMapping("/intro_audio/add")
    public void addIntroAudio(@RequestBody IntroAudio introAudio) {
        audioService.addIntroAudio(introAudio);
    }

    @DeleteMapping("/intro_audio/delete/{id}")
    public boolean deleteIntroAudio(@PathVariable Integer id) {
        audioService.deleteIntroAudio(id);
        return true;
    }

    @PostMapping("/intro_audio/edit")
    public void editIntroAudio(@RequestBody IntroAudio introAudio) {
        audioService.editIntroAudio(introAudio);
    }

    @GetMapping("/comment/agree/{id}")
    public boolean verifyComment(@PathVariable Integer id) {
        commentService.verifyComment(id);
        return true;
    }

    @PostMapping("/shop/add")
    public boolean addShop(@RequestBody Shop shop) {
        sellerService.addShop(shop);
        return true;
    }

    @DeleteMapping("/shop/delete/{shop_id}")
    public boolean editShop(@PathVariable Integer shop_id) {
        sellerService.deleteShop(shop_id);
        return true;
    }

    @PostMapping("/tour_route/add")
    public boolean addTourRoute(@RequestBody TourRoute tourRoute) {
        tourRouteService.addTourRoute(tourRoute);
        return true;
    }

    @PostMapping("/tour_route/edit")
    public boolean editTourRoute(@RequestBody TourRoute tourRoute) {
        tourRouteService.editTourRoute(tourRoute);
        return true;
    }

    @DeleteMapping("/tour_route/delete/{tour_route_id}")
    public void deleteTourRoute(@PathVariable Integer tour_route_id) {
        tourRouteService.deleteTourRoute(tour_route_id);
    }

}
