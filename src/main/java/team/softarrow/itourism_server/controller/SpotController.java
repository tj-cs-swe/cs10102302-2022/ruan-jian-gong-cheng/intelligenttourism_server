package team.softarrow.itourism_server.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import team.softarrow.itourism_server.entity.Spot;
import team.softarrow.itourism_server.entity.form.BuyTicketForm;
import team.softarrow.itourism_server.service.SpotService;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/spot")
public class SpotController {

    private SpotService spotService;

    @Autowired
    public void setSpotService(SpotService spotService) {
        this.spotService = spotService;
    }

    @PostMapping("/buy")
    public Map<String, String> buyTicket(@RequestBody @Validated BuyTicketForm buyTicketForm) throws Exception {
        return spotService.buyTicket(buyTicketForm);
    }

    @GetMapping("/return_ticket/{id}")
    public void returnTicket(@PathVariable Long id) throws Exception {
        spotService.returnTicket(id);
    }

    @GetMapping("/{id}")
    public Spot getSpot(@PathVariable Integer id) {
        return spotService.getSpotInfo(id);
    }

    /**
     * 查询景区
     */
    @GetMapping("/find/{name}")
    public Spot find(@PathVariable(name = "name")String name){
        return spotService.findByName(name);
    }

    @GetMapping("/list/{page}/{size}")
    public List<Spot> getSpotList(@PathVariable Integer page, @PathVariable Integer size) {
        return spotService.getSpotList(page, size);
    }
}
