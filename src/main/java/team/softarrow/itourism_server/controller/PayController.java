package team.softarrow.itourism_server.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import team.softarrow.itourism_server.common.utils.PaymentUtils;
import team.softarrow.itourism_server.entity.Order;
import team.softarrow.itourism_server.service.PayService;

import javax.servlet.http.HttpServletRequest;
import java.nio.charset.StandardCharsets;
import java.util.Map;

@Controller
@RequestMapping("/pay")
public class PayController {

    private PayService payService;

    @Autowired
    public void setPayService(PayService payService) {
        this.payService = payService;
    }

    @PostMapping("/result")
    public void payCallBack(HttpServletRequest request) throws Exception {
        Map<String,String[]> requestParams = request.getParameterMap();
        PaymentUtils.verifyNotify(requestParams);

        String out_trade_no = new String(request
                .getParameter("out_trade_no")
                .getBytes(StandardCharsets.ISO_8859_1), StandardCharsets.UTF_8);
        payService.updateOrder(Long.parseLong(out_trade_no), Order.OrderState.Paid);
    }
}
