package team.softarrow.itourism_server.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import team.softarrow.itourism_server.entity.Spot;
import team.softarrow.itourism_server.entity.User;
import team.softarrow.itourism_server.service.SpotService;
import team.softarrow.itourism_server.service.UserService;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/admin")
public class AdminController {

    private UserService userService;
    private SpotService spotService;

    @Autowired
    public void setSpotService(SpotService spotService) {
        this.spotService = spotService;
    }

    @Autowired
    public void setVisitorService(UserService userService) {
        this.userService = userService;
    }

    /**
     * 删除景区
     */
    @DeleteMapping("/spot/delete/{id}")
    public void deleteSpot(@PathVariable(name="id")Integer id){
        spotService.deleteSpot(id);
    }

    /**
     * 添加景区
     */
    @PostMapping("/spot/add")
    public boolean addSpot(@RequestBody Spot spot){
        spotService.addSpot(spot);
        return true;
    }

    @GetMapping("/permission/list/{page}/{size}")
    public List<User> getUserList(@PathVariable Integer page, @PathVariable Integer size) {
        return userService.getUserList(page, size);
    }

    @PostMapping("/permission/add")
    public boolean setUserPermission(@RequestBody Map<String, Integer> info) {
        userService.changeUserPermission(info);
        return true;
    }
}
