package team.softarrow.itourism_server.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import team.softarrow.itourism_server.entity.*;
import team.softarrow.itourism_server.service.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

@RestController
@RequestMapping("/travel")
public class TravelController {

    private ServicePointService servicePointService;
    private CommentService commentService;
    private AudioService audioService;
    private UploadService uploadService;
    private TourRouteService tourRouteService;
    private SpotService spotService;

    @Autowired
    public void setSpotService(SpotService spotService) {
        this.spotService = spotService;
    }

    @Autowired
    public void setTourRouteService(TourRouteService tourRouteService) {
        this.tourRouteService = tourRouteService;
    }

    @Autowired
    public void setUploadService(UploadService uploadService) {
        this.uploadService = uploadService;
    }

    @Autowired
    public void setAudioService(AudioService audioService) {
        this.audioService = audioService;
    }

    @Autowired
    public void setCommentService(CommentService commentService) {
        this.commentService = commentService;
    }

    @Autowired
    public void setServicePointService(ServicePointService servicePointService) {
        this.servicePointService = servicePointService;
    }

    @GetMapping("/comment/{id}/{page}/{size}")
    public List<Comment> getCommentList(@PathVariable Integer id,
                                        @PathVariable Integer page,
                                        @PathVariable Integer size) {
        return commentService.getCommentInServicePoint(id, page, size);
    }

    @GetMapping("/comment/{id}")
    public Comment getCommentInfo(@PathVariable Integer id) {
        return commentService.getCommentById(id);
    }

    @PostMapping("/comment/add")
    public void addComment(@RequestBody Comment comment) {
        commentService.addComment(comment);
    }

    @DeleteMapping ("/comment/delete/{cid}")
    public void deleteComment(@PathVariable Integer cid) {
        commentService.deleteComment(cid);
    }

    @PostMapping("/comment/like/{cid}")
    public boolean commentLike(@PathVariable Integer cid) {
        return commentService.commentLike(cid);
    }

    @PostMapping("/comment/undo-like/{cid}")
    public boolean commentUnDoLike(@PathVariable Integer cid) {
        return commentService.commentUnDoLike(cid);
    }

    @PostMapping("/comment/dislike/{cid}")
    public void commentDisLike(@PathVariable Integer cid) {
        commentService.commentDisLike(cid);
    }

    @PostMapping("/comment/undo-dislike/{cid}")
    public void commentUndoDisLike(@PathVariable Integer cid) {
        commentService.commentUndoDisLike(cid);
    }

    @GetMapping("/service_point/list/{spot_id}/{page}/{size}")
    public List<ServicePoint> getServicePointList(@PathVariable Integer spot_id,
                                                  @PathVariable Integer page,
                                                  @PathVariable Integer size) {
        return servicePointService.getServicePointList(spot_id, page ,size);
    }

    @GetMapping("/service_point/all/{spot_id}")
    public List<ServicePoint> getAllServicePoint(@PathVariable Integer spot_id) {
        return servicePointService.getAllServicePoint(spot_id);
    }

    @GetMapping("/service_point/{service_point_id}")
    public ServicePoint getServicePointInfo(@PathVariable Integer service_point_id) {
        return servicePointService.getServicePointInfo(service_point_id);
    }

    /**
     * 根据url获取音频文件
     */
    @GetMapping("/intro_audio/download/{id}")
    public void download(@PathVariable Integer id, HttpServletResponse response) {
        audioService.downloadAudio(id,response);
    }

    @GetMapping("/intro_audio/{id}")
    public IntroAudio getAudio(@PathVariable Integer id) {
        return audioService.getIntroAudioInfo(id);
    }

    @GetMapping("/intro_audio/list/{service_point_id}/{page}/{size}")
    public List<IntroAudio> getIntroAudioInServicePoint(@PathVariable Integer service_point_id,
                                                        @PathVariable Integer page,
                                                        @PathVariable Integer size){
        return audioService.getIntroAudioInServicePoint(service_point_id,page,size);
    }

    @PostMapping("/upload/image")
    public Picture uploadImage(@RequestPart(value = "file") MultipartFile file) {
        return uploadService.uploadPicture(file);
    }

    @GetMapping("/tour_route/get/{tid}")
    public List<ServicePoint> getAsArrayList(@PathVariable Integer tid){
        return tourRouteService.getRouteInfo(tid);
    }

    @GetMapping("/tour_route/list/{spot_id}/{page}/{size}")
    public List<TourRoute> getTourRoutelist(@PathVariable Integer spot_id,
                                            @PathVariable Integer page,
                                            @PathVariable Integer size) {
        return tourRouteService.getTourRouteList(spot_id, page, size);
    }

    @GetMapping("/score/{id}/{score}")
    public void scoreSpot(@PathVariable Integer id, @PathVariable Integer score) {
        spotService.scoreSpot(id, score);
    }
}
