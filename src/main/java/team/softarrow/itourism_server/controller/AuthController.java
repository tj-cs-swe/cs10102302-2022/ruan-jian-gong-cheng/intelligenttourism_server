package team.softarrow.itourism_server.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import team.softarrow.itourism_server.common.captcha.ImageCode;
import team.softarrow.itourism_server.security.VisitorDetail;
import team.softarrow.itourism_server.service.AuthService;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.Map;

@RestController
@RequestMapping("/auth")
public class AuthController {
    private AuthService authService;

    @Autowired
    public void setAuthService(AuthService authService) {
        this.authService = authService;
    }

    @GetMapping("/captcha")
    public ImageCode sendImageCode() throws IOException {
        ImageCode captcha = authService.createImageCode();
        captcha.setText(null);      // 抹去明文验证码
        return captcha;
    }

    @PostMapping("/email")
    public boolean sendEmail(@RequestBody @Validated VisitorDetail detail) {
        authService.sendEmail(detail.getEmail());
        return true;
    }

    @PostMapping("/register")
    public Map<String, String> register(HttpServletRequest request, @Validated @RequestBody VisitorDetail visitorDetail) {
        return authService.registerUser(request, visitorDetail);
    }

    @PostMapping("forget")
    public boolean forget(@RequestBody @Validated VisitorDetail detail) {
        authService.forgetPassword(detail);
        return true;
    }

}
