package team.softarrow.itourism_server.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import team.softarrow.itourism_server.entity.Order;
import team.softarrow.itourism_server.entity.Ticket;
import team.softarrow.itourism_server.entity.User;
import team.softarrow.itourism_server.service.UserService;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/mine")
public class UserController {

    private UserService userService;

    @Autowired
    public void setVisitorService(UserService userService) {
        this.userService = userService;
    }

    /**
     * 修改密码
     * @param info
     * @return
     */
    @PostMapping("/change-password")
    public boolean changePassword(@RequestBody Map<String, String> info) {
        userService.changePassword(info);
        return true;
    }

    /**
     * 修改个人信息
     */
    @PostMapping("/edit")
    public User edit(@RequestBody Map<String, String> change){
        return userService.edit(change);
    }

    /**
     * 用户上传头像
     * @return
     */
    @PostMapping(value = "/avatar/upload")
    public boolean uploadAvatar(@RequestBody Map<String, String> avatar){
        userService.uploadAvatar(avatar.get("base64"));
        return true;
    }

    @GetMapping()
    public User getUser() {
        return userService.getCurrentUserInfo();
    }

    @GetMapping("/goods/orders/{page}/{size}")
    public List<Order> getMyCommodityOrders(@PathVariable Integer page, @PathVariable Integer size) {
        return userService.getCommodityOrderBelongMe(page, size);
    }

    @GetMapping("/tickets/orders/{page}/{size}")
    public List<Order> getMyTicketOrders(@PathVariable Integer page, @PathVariable Integer size) {
        return userService.getTicketOrderBelongMe(page, size);
    }

    @GetMapping("/tickets/{page}/{size}")
    public List<Ticket> getMyTickets(@PathVariable Integer page, @PathVariable Integer size) {
        return userService.getTicketBelongMe(page, size);
    }

}
