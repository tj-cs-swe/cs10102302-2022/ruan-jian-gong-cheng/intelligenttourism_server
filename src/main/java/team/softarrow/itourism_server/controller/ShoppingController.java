package team.softarrow.itourism_server.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import team.softarrow.itourism_server.entity.Commodity;
import team.softarrow.itourism_server.entity.Shop;
import team.softarrow.itourism_server.entity.form.BuyCommodityForm;
import team.softarrow.itourism_server.service.PayService;
import team.softarrow.itourism_server.service.ShoppingService;
import team.softarrow.itourism_server.service.SpotService;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/shop")
public class ShoppingController {

    private SpotService spotService;
    private ShoppingService shoppingService;
    private PayService payService;

    @Autowired
    public void setPayService(PayService payService) {
        this.payService = payService;
    }

    @Autowired
    public void setSpotService(SpotService spotService) {
        this.spotService = spotService;
    }

    @Autowired
    public void setShopService(ShoppingService shoppingService) {
        this.shoppingService = shoppingService;
    }

    @GetMapping("/scene/{sid}/{page}/{size}")
    public List<Shop> getShopsInScene(@PathVariable Integer sid,
                                     @PathVariable Integer page,
                                     @PathVariable Integer size) {
        return spotService.getShopsInSpot(sid, page, size);
    }

    @GetMapping("/store/{id}/{page}/{size}")
    public List<Commodity> getCommoditiesInShop(@PathVariable Integer id,
                                                @PathVariable Integer page,
                                                @PathVariable Integer size) {
        return shoppingService.getCommoditiesInShop(id, page, size);
    }

    @GetMapping("/{id}")
    public Shop getShopInfo(@PathVariable Integer id) {
        return shoppingService.getShopInfo(id);
    }

    @GetMapping("/item/{id}")
    public Commodity getCommodityInfo(@PathVariable Integer id) {
        return shoppingService.getCommodityInfo(id);
    }

    @PostMapping("/buy")
    public Map<String, String> buyCommodity(@RequestBody @Validated BuyCommodityForm buyCommodityForm) throws Exception {
        return shoppingService.buyCommodity(buyCommodityForm);
    }

    @GetMapping("/refund/{id}")
    public void returnCommodity(@PathVariable Long id) throws Exception {
        shoppingService.returnCommodity(id);
    }

    @GetMapping("/finish/{id}")
    public void finishOrder(@PathVariable Long id) {
        shoppingService.finishOrder(id);
    }

    @GetMapping("/pay/{id}")
    public Map<String, String> payOrder(@PathVariable Long id) throws Exception {
        return payService.payOrder(id);
    }
}
