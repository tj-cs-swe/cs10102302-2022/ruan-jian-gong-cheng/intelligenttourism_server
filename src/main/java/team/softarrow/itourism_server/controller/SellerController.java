package team.softarrow.itourism_server.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import team.softarrow.itourism_server.entity.Commodity;
import team.softarrow.itourism_server.entity.Order;
import team.softarrow.itourism_server.entity.Shop;
import team.softarrow.itourism_server.service.SellerService;

import java.util.List;

@RestController
@RequestMapping("/shop")
public class SellerController {

    private SellerService sellerService;

    @Autowired
    public void setSellerService(SellerService sellerService) {
        this.sellerService = sellerService;
    }

    @PostMapping("/goods/add")
    public void addCommodity(@RequestBody Commodity commodity) {
        sellerService.addCommodity(commodity);
    }

    @PostMapping("/goods/edit")
    public void editCommodity(@RequestBody Commodity commodity) {
        sellerService.editCommodity(commodity);
    }

    @DeleteMapping("/goods/delete/{id}")
    public void deleteCommodity(@PathVariable Integer id) {
        sellerService.deleteCommodity(id);
    }

    @PostMapping("/edit")
    public void editShop(@RequestBody Shop shop) {
        sellerService.editShop(shop);
    }

    @PostMapping("/order/deliver/{id}")
    public void deliver(@PathVariable Long id) {
        sellerService.deliverCommodity(id);
    }

    @GetMapping("/order/list/{page}/{size}")
    public List<Order> getOrdersInShop(@PathVariable Integer page,
                                       @PathVariable Integer size) {
        return sellerService.getOrdersInShop(page, size);
    }
}
