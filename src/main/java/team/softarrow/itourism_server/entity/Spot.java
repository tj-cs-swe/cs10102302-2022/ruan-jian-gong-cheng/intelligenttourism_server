package team.softarrow.itourism_server.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.LazyGroup;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.Set;

@Setter
@Getter
@RequiredArgsConstructor
@Entity
@Table(name = "spots")
@Where(clause = "deleted=0")
public class Spot {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Integer id;

    @Column(length=128,name = "name", nullable = false)
    @Size(min = 1, max = 128)
    private String name;

    @Column(nullable = false)
    @Size(min = 1, max = 1024)
    private String position;

    @Lob
    @Basic(fetch = FetchType.LAZY)
    @Column(columnDefinition="Longtext", nullable = false)
    @LazyGroup(value = "spot_figure")
    private String figure;

    @Lob
    @Basic(fetch = FetchType.LAZY)
    @Column(columnDefinition="Longtext", nullable = false)
    @LazyGroup(value = "spot_introduction")
    @Size(min = 1, max = 1024)
    private String introduction;

    @Column(nullable = false)
    private Long leftTickets;

    @Column(nullable = false)
    private Double ticketPrice;

    @Column(nullable = false)
    private Double score;

    @Transient
    @JsonProperty
    private boolean ifScore;

    @Transient
    @JsonProperty
    private boolean existScore;

    @Column(nullable = false)
    private boolean deleted;

    @OneToMany(mappedBy = "spot", cascade = CascadeType.ALL)
    private Set<ServicePoint> servicePoints;

    @OneToMany(mappedBy = "spot", cascade = CascadeType.ALL)
    private Set<Ticket> tickets;

    @OneToMany(mappedBy = "spot", cascade = CascadeType.ALL)
    private Set<TourRoute> tourRoutes;

    @OneToMany(mappedBy = "adminSpot",
            cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    private Set<User> spotAdmin;

    @OneToMany(mappedBy = "spot",
            cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    private Set<Shop> shops;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable()
    private Set<User> evaluate;

}
