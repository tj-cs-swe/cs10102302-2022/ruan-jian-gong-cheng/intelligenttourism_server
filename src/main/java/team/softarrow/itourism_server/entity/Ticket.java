package team.softarrow.itourism_server.entity;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.Date;

@Setter
@Getter
@RequiredArgsConstructor
@Entity
@Table(name = "tickets")
@NamedEntityGraph(name = "Ticket.Graph", attributeNodes = {@NamedAttributeNode("spot")})
public class Ticket {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Integer id;

    @Column(name = "price",nullable = false)
    private Double price;

    @Column(name = "buy_time", nullable = false)
    private Date buyTime;

    @Column(nullable = false)
    private Date useTime;

    @Column(nullable = false)
    private TicketState ticketState;

    @Column(length = 32, nullable = false)
    @Size(min = 1, max = 32)
    private String idCard;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    private User user;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "spot_id")
    private Spot spot;

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE},
                fetch = FetchType.LAZY)
    @JoinColumn(name = "order_id")
    private Order order;

    public enum TicketState {
        Pending, Paid, Finish, Refund
    }
}
