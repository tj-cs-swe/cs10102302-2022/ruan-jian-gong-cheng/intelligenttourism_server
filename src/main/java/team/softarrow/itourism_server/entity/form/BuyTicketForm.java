package team.softarrow.itourism_server.entity.form;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import javax.validation.constraints.Future;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.Set;

@Data
public class BuyTicketForm {
    @NotNull
    private Integer spotId;
    @Min(value = 1, message = "数量有误")
    @Max(value = 65536, message = "数量有误")
    private int amount;
    @NotNull
    private Set<String> idCard;
    @Future
    @NotNull
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date useTime;
}
