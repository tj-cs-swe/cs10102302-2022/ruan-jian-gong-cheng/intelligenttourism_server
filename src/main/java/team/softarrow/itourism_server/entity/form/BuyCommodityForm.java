package team.softarrow.itourism_server.entity.form;

import lombok.Data;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Data
public class BuyCommodityForm {
    @NotNull
    private Integer id;
    @Min(value = 1, message = "数量有误")
    @Max(value = 65536, message = "数量有误")
    private int amount;
}
