package team.softarrow.itourism_server.entity;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;
import java.util.Set;

@Setter
@Getter
@RequiredArgsConstructor
@Entity
@Table(name = "orders")
@NamedEntityGraph(name = "TicketOrder.Graph",
        attributeNodes = {@NamedAttributeNode("spot")})
@NamedEntityGraph(name = "CommodityOrder.Graph",
        attributeNodes = {@NamedAttributeNode("commodity")})
public class Order {
    @Id
    @Column(nullable = false)
    private Long id;

    @Column(nullable = false)
    private Integer amount;

    @Column(nullable = false)
    private Double price;

    @Column(nullable = false)
    private Date createTime;

    @Column(nullable = false)
    private OrderState orderState;

    @Column(nullable = false)
    private OrderType orderType;

    @OneToOne(cascade = {CascadeType.MERGE, CascadeType.PERSIST}, fetch = FetchType.LAZY)
    @JoinColumn(name = "commodity_id")
    private Commodity commodity;

    @OneToMany(mappedBy = "order", cascade = CascadeType.ALL)
    private Set<Ticket> tickets;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    private User user;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "spot_id")
    private Spot spot;

    public enum OrderState {
        Pending, Paid, Transiting, Finish, Refund
    }

    public enum OrderType {
        Ticket, Commodity
    }
}
