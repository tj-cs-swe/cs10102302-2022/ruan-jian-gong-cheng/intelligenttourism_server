package team.softarrow.itourism_server.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.Date;
import java.util.Set;

@Setter
@Getter
@RequiredArgsConstructor
@Entity
@Table(name = "comments")
@Where(clause = "deleted=0")
@NamedEntityGraph(name = "Comment.Graph",
        attributeNodes = {@NamedAttributeNode("users"),
                            @NamedAttributeNode("downUsers"),
                            @NamedAttributeNode("pictures"),
                            @NamedAttributeNode("user")})
public class Comment {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Integer id;

    @Column(name = "time", nullable = false)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date time;

    @Column(nullable = false)
    private Integer likeNum;

    @Column(nullable = false)
    private boolean top;

    @Column(nullable = false)
    private boolean verified;

    @Column(nullable = false)
    private boolean deleted;

    @Transient
    @JsonProperty
    private boolean ifLike;

    @Transient
    @JsonProperty
    private boolean ifDislike;

    @Lob
    @Column(columnDefinition = "text", nullable = false)
    @Size(min = 1, max = 1024)
    private String contain;

    @OneToMany(cascade = { CascadeType.MERGE, CascadeType.PERSIST} ,fetch = FetchType.EAGER)
    @JoinTable()
    private Set<Picture> pictures;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "service_id")
    private ServicePoint servicePoint;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    private User user;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable()
    private Set<User> users;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable()
    private Set<User> downUsers;

}
