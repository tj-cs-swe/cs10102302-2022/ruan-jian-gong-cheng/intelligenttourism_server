package team.softarrow.itourism_server.entity;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.Objects;
import java.util.Set;

@Setter
@Getter
@RequiredArgsConstructor
@Entity
@Table(name = "users")
@NamedEntityGraph(name = "User.Graph",
        attributeNodes = {@NamedAttributeNode("permissions")})
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(length = 128, nullable = false)
    @Size(min = 1, max = 128)
    private String username;

    @Column(nullable = false)
    private String password;

    @Column(length = 64, nullable = false)
    @Size(min = 1, max = 64)
    private String email;

    @Column(length = 64)
    @Size(min = 1, max = 64)
    private String phone;

    @Column()
    private String gender;

    @Column()
    private Integer age;

    @Column(length = 32, nullable = false)
    @Size(min = 1, max = 32)
    private String ip;

    @Column(nullable = false)
    private String avatar;

    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL)
    private Set<Ticket> tickets;

    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL)
    private Set<Comment> comments;

    @ManyToMany()
    @JoinTable(name = "user_perms")
    private Set<Permission> permissions;

    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL)
    private Set<Order> orders;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "spot_admin_id")
    private Spot adminSpot;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "shop_id")
    private Shop shop;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        User user = (User) o;

        return Objects.equals(id, user.id);
    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }
}

