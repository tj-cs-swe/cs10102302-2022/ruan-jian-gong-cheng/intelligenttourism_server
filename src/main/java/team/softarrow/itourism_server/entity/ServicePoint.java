package team.softarrow.itourism_server.entity;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.LazyGroup;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.Set;

/**
 * 服务点实体类
 *
 * @author Oscar_Wen
 */
@Setter
@Getter
@RequiredArgsConstructor
@Entity
@Table(name = "service_points")
@Where(clause = "deleted=0")
public class ServicePoint {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(length = 64, nullable = false)
    @Size(min = 1, max = 64)
    private String name;

    @Column(length = 64, nullable = false)
    @Size(min = 1, max = 64)
    private String type;

    @Lob
    @Basic(fetch = FetchType.LAZY)
    @Column(columnDefinition="Longtext", nullable = false)
    @LazyGroup("sp_figure")
    private String figure;

    @Lob
    @Basic(fetch = FetchType.LAZY)
    @Column(columnDefinition = "text", nullable = false)
    @LazyGroup("sp_introduce")
    @Size(min = 1, max = 1024)
    private String introduce;

    @Column(nullable = false)
    @Size(min = 1, max = 1024)
    private String position;

    @Column(nullable = false)
    private boolean deleted;

    @OneToMany(mappedBy = "servicePoint", cascade = CascadeType.ALL)
    private Set<IntroAudio> introAudios;

    @OneToMany(mappedBy = "servicePoint", cascade = CascadeType.ALL)
    private Set<Comment> comments;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "spot_id")
    private Spot spot;
}
