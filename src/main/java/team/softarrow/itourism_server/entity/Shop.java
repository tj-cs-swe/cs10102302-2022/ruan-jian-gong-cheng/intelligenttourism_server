package team.softarrow.itourism_server.entity;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.LazyGroup;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.Set;

@Setter
@Getter
@RequiredArgsConstructor
@Entity
@Table(name = "shops")
@Where(clause = "deleted=0")
public class Shop {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false)
    private Integer id;

    @Column(nullable = false)
    @Size(min = 1, max = 128)
    private String name;

    @Column(nullable = false)
    @Size(min = 1, max = 256)
    private String brief;

    @Column(nullable = false)
    @Size(min = 1, max = 1024)
    private String position;

    @Lob
    @Basic(fetch = FetchType.LAZY)
    @LazyGroup("shop_figure")
    @Column(columnDefinition="Longtext", nullable = false)
    private String figure;

    @Lob
    @Basic(fetch = FetchType.LAZY)
    @LazyGroup("shop_introduction")
    @Column(columnDefinition="Longtext", nullable = false)
    @Size(min = 1, max = 1024)
    private String introduction;

    @Column(nullable = false)
    private boolean deleted;

    @OneToMany(mappedBy = "shop", cascade=CascadeType.ALL)
    private Set<Commodity> commodities;

    @OneToMany(mappedBy = "shop")
    private Set<User> sellers;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "spot_id")
    @LazyGroup("spot1")
    private Spot spot;
}
