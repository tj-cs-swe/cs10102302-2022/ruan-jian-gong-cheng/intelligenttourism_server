package team.softarrow.itourism_server.entity;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.LazyGroup;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import javax.validation.constraints.Size;

@Setter
@Getter
@RequiredArgsConstructor
@Entity
@Table(name = "commodities")
@Where(clause = "deleted=0")
public class Commodity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Integer id;

    @Column(name = "name", nullable = false, length = 128)
    @Size(min = 1, max = 128)
    private String name;

    @Lob
    @Basic(fetch = FetchType.LAZY)
    @LazyGroup("commodity_figure")
    @Column(columnDefinition="Longtext", nullable = false)
    private String figure;

    @Lob
    @Basic(fetch = FetchType.LAZY)
    @LazyGroup("commodity_introduction")
    @Column(columnDefinition="Longtext", nullable = false)
    @Size(min = 1, max = 1024)
    private String introduction;

    @Column(name = "price", nullable = false)
    private Double price;

    @Column(nullable = false)
    private Long sales;

    @Column(nullable = false)
    private boolean deleted;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "shop_id")
    @LazyGroup(value = "commodity_shop")
    private Shop shop;
}
