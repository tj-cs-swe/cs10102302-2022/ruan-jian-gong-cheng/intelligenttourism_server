package team.softarrow.itourism_server.entity;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import javax.validation.constraints.Size;

/**
 * 解说音频实体类
 *
 * @author Oscar_Wen
 */
@Setter
@Getter
@RequiredArgsConstructor
@Entity
@Table(name = "intro_audios")
@Where(clause = "deleted=0")
public class IntroAudio {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(length = 64, nullable = false)
    @Size(min = 1, max = 64)
    private String name;

    @Column(length = 256,nullable = false)
    @Size(min = 1, max = 256)
    private String url;

    @Lob
    @Basic(fetch = FetchType.LAZY)
    @Column(columnDefinition = "text", nullable = false)
    @Size(min = 1, max = 1024)
    private String introduce;

    @Column(nullable = false)
    private boolean deleted;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "point_id")
    private ServicePoint servicePoint;
}
