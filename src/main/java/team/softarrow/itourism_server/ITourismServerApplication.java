package team.softarrow.itourism_server;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ITourismServerApplication {

    public static void main(String[] args) {
        SpringApplication.run(ITourismServerApplication.class, args);
    }

}
