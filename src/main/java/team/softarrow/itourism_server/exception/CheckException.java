package team.softarrow.itourism_server.exception;

import org.springframework.security.core.AuthenticationException;
import team.softarrow.itourism_server.response.ResponseCode;

public class CheckException extends AuthenticationException {

    private static final long serialVersionUID = 1866845819231938587L;

    private ResponseCode  code;

    public CheckException(ResponseCode responseCode){
        super(responseCode.getMessage());
        this.code = responseCode;
    }

    public CheckException(String message) {
        super(message);
        code = ResponseCode.ACCESS_DENY;
    }

    public ResponseCode getCode() {
        return code;
    }

    public void setCode(ResponseCode code) {
        this.code = code;
    }
}
