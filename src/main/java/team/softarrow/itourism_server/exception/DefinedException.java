package team.softarrow.itourism_server.exception;

import team.softarrow.itourism_server.response.ResponseCode;

public class DefinedException extends RuntimeException {

    private static final long serialVersionUID = -6715919594638688928L;

    private ResponseCode code;

    public DefinedException(ResponseCode responseCode){
        super(responseCode.getMessage());
        this.code = responseCode;
    }

    public DefinedException(ResponseCode responseCode, String message) {
        super(message);
        this.code = responseCode;
    }

    public ResponseCode getCode() {
        return code;
    }

    public void setCode(ResponseCode code) {
        this.code = code;
    }

}
