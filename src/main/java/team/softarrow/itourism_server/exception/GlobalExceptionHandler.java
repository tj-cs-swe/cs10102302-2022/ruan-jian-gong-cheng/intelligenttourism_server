package team.softarrow.itourism_server.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import team.softarrow.itourism_server.response.ResponseCode;
import team.softarrow.itourism_server.response.ReturnBody;

/**
 * 全局异常处理器
 */
@RestControllerAdvice
public class GlobalExceptionHandler {
    /**
     * 自定义异常 RuntimeException
     * @param e
     * @return
     */
    @ExceptionHandler(value = DefinedException.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public ReturnBody exceptionHandle(DefinedException e){
        return ReturnBody.fail(e.getCode(), e.getMessage());
    }

    @ExceptionHandler(value = MethodArgumentNotValidException.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public ReturnBody handleArgument(MethodArgumentNotValidException e) {
        String error = e.getBindingResult().getAllErrors().get(0).getDefaultMessage();
        return ReturnBody.fail(ResponseCode.NOT_MATCH, error);
    }

    /**
     * 全部异常
     * @param e
     * @return
     */
    @ExceptionHandler(value = Exception.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public ReturnBody handleNullPointer(Exception e) {
        return ReturnBody.fail(ResponseCode.SERVER_ERROR, e.getMessage());
    }
}
