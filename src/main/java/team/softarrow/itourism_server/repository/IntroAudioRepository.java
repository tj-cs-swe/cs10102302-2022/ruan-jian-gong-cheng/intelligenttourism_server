package team.softarrow.itourism_server.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;
import team.softarrow.itourism_server.entity.IntroAudio;
import team.softarrow.itourism_server.entity.ServicePoint;

public interface IntroAudioRepository extends JpaRepository<IntroAudio,Integer> {

    @Modifying
    @Transactional
    @Query(value = "update intro_audios set deleted=true where id=?1", nativeQuery = true)
    void deleteIntroAudio(Integer id);

    Page<IntroAudio> findByServicePoint(ServicePoint servicePoint, Pageable pageable);
}
