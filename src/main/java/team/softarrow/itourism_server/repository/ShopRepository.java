package team.softarrow.itourism_server.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import team.softarrow.itourism_server.entity.Shop;
import team.softarrow.itourism_server.entity.Spot;

public interface ShopRepository extends JpaRepository<Shop, Integer> {

    Page<Shop> findBySpot(Spot spot, Pageable pageable);
}
