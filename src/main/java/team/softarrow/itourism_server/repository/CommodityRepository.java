package team.softarrow.itourism_server.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;
import team.softarrow.itourism_server.entity.Commodity;
import team.softarrow.itourism_server.entity.Shop;

public interface CommodityRepository extends JpaRepository<Commodity, Integer> {
    Page<Commodity> findByShop(Shop shop, Pageable pageable);

    @Query(value = "select sales from commodities where id=?1", nativeQuery = true)
    Long getSale(Integer id);

    @Modifying
    @Transactional
    @Query(value = "update commodities set sales=sales+?2 where id=?1", nativeQuery = true)
    void increaseSale(int id, int sale);

    @Modifying
    @Transactional
    @Query(value = "update commodities set sales=sales-?2 where id=?1", nativeQuery = true)
    void decreaseSale(int id, int sale);

    @Modifying
    @Transactional
    @Query(value = "update commodities set deleted=true where id=?1", nativeQuery = true)
    void deleteCommodity(int id);
}
