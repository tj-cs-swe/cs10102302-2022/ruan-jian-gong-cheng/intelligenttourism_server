package team.softarrow.itourism_server.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;
import team.softarrow.itourism_server.entity.Order;
import team.softarrow.itourism_server.entity.Shop;
import team.softarrow.itourism_server.entity.User;

public interface OrderRepository extends JpaRepository<Order, Long> {

    @EntityGraph(value = "TicketOrder.Graph", type = EntityGraph.EntityGraphType.FETCH)
    @Query("select o from Order o where o.user=?1 and o.orderType=0")
    Page<Order> findTicketOrders(User user, Pageable pageable);

    @EntityGraph(value = "CommodityOrder.Graph", type = EntityGraph.EntityGraphType.FETCH)
    @Query("select o from Order o where o.user=?1 and o.orderType=1")
    Page<Order> findCommodityOrders(User user, Pageable pageable);

    @EntityGraph(value = "CommodityOrder.Graph", type = EntityGraph.EntityGraphType.FETCH)
    @Query("select o from Order o where o.commodity.shop=?1 and o.orderType=1")
    Page<Order> findShopOrders(Shop shop, Pageable pageable);

    @Modifying
    @Transactional
    @Query(value = "update orders set order_state=?2 where id=?1", nativeQuery = true)
    void setState(Long id, int state);
}
