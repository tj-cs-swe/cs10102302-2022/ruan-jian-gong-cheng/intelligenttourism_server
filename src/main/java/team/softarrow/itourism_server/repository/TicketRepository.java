package team.softarrow.itourism_server.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;
import team.softarrow.itourism_server.entity.Ticket;
import team.softarrow.itourism_server.entity.User;

public interface TicketRepository extends JpaRepository<Ticket, Integer> {
    @EntityGraph(value = "Ticket.Graph", type = EntityGraph.EntityGraphType.FETCH)
    Page<Ticket> findByUser(User user, Pageable pageable);

    @Modifying
    @Transactional
    @Query(value = "update tickets set ticket_state=?2 where id=?1", nativeQuery = true)
    void setState(int id, int state);
}
