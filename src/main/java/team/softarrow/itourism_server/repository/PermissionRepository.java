package team.softarrow.itourism_server.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import team.softarrow.itourism_server.entity.Permission;

public interface PermissionRepository extends JpaRepository<Permission, Integer> {
}
