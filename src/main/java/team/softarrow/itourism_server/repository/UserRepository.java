package team.softarrow.itourism_server.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import team.softarrow.itourism_server.entity.User;

import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, Integer> {

    @EntityGraph(value = "User.Graph", type = EntityGraph.EntityGraphType.FETCH)
    @Query(value = "select u from User u")
    Page<User> findAllUser(Pageable pageable);

    Optional<User> findVisitorByUsername(String username);

    Optional<User> getByEmail(String email);

    @Modifying
    @Transactional
    @Query(value = "update users set ip=?2 where id=?1", nativeQuery = true)
    void updateIP(long id, String ip);

    @Modifying
    @Transactional
    @Query(value = "update users set avatar=?2 where id=?1", nativeQuery = true)
    void updateAvatar(int id, String avatar);
}
