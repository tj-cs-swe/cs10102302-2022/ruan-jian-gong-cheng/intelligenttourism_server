package team.softarrow.itourism_server.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import team.softarrow.itourism_server.entity.ServicePoint;
import team.softarrow.itourism_server.entity.Spot;

import java.util.List;

public interface ServicePointRepository extends JpaRepository<ServicePoint, Integer> {

    Page<ServicePoint> findBySpot(Spot spot, Pageable pageable);

    List<ServicePoint> findAllBySpot(Spot spot);
}
