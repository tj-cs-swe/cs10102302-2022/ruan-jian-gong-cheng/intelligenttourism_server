package team.softarrow.itourism_server.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;
import team.softarrow.itourism_server.entity.Comment;
import team.softarrow.itourism_server.entity.ServicePoint;

public interface CommentRepository extends JpaRepository <Comment,Integer>{

    @Modifying
    @Transactional
    @Query(value = "update comments set deleted=true where id=?1", nativeQuery = true)
    void deleteComment(Integer id);

    @Modifying
    @Transactional
    @Query(value = "update comments set top=true where id=?1", nativeQuery = true)
    void topComment(Integer id);

    @Modifying
    @Transactional
    @Query(value = "update comments set verified=true where id=?1", nativeQuery = true)
    void verifyComment(Integer id);

    @EntityGraph(value = "Comment.Graph", type = EntityGraph.EntityGraphType.FETCH)
    @Query(value = "select c from Comment c where c.verified=true and c.servicePoint=?1")
    Page<Comment> findByServicePoint(ServicePoint servicePoint, Pageable pageable);

    @EntityGraph(value = "Comment.Graph", type = EntityGraph.EntityGraphType.FETCH)
    Page<Comment> findAllCommentByServicePoint(ServicePoint servicePoint, Pageable pageable);
}
