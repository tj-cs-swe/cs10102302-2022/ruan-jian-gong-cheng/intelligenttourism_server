package team.softarrow.itourism_server.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;
import team.softarrow.itourism_server.entity.Spot;

import java.util.Optional;

public interface SpotRepository extends JpaRepository<Spot, Integer> {
    @Modifying
    @Transactional
    @Query(value = "update spots set left_tickets=left_tickets-?2 where id=?1", nativeQuery = true)
    void decreaseTicket(int id, int size);

    @Modifying
    @Transactional
    @Query(value = "update spots set left_tickets=left_tickets+?2 where id=?1", nativeQuery = true)
    void increaseTicket(int id, int size);

    Optional<Spot> findByName(String name);

    Optional<Spot> findSpotById(int id);
}
