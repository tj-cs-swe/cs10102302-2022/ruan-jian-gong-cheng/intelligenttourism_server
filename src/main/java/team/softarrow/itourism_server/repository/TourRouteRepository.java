package team.softarrow.itourism_server.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;
import team.softarrow.itourism_server.entity.Spot;
import team.softarrow.itourism_server.entity.TourRoute;

public interface TourRouteRepository extends JpaRepository<TourRoute, Integer> {
    Page<TourRoute> findBySpot(Spot spot, Pageable pageable);

    @Modifying
    @Transactional
    @Query(value = "update tour_routes set deleted=true where id=?1", nativeQuery = true)
    void deleteTourRoute(Integer id);
}
