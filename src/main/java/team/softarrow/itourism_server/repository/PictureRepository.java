package team.softarrow.itourism_server.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import team.softarrow.itourism_server.entity.Picture;

public interface PictureRepository extends JpaRepository<Picture, Integer> {
}
