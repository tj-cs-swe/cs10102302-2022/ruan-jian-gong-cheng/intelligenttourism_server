package team.softarrow.itourism_server.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import team.softarrow.itourism_server.common.utils.PaymentUtils;
import team.softarrow.itourism_server.entity.Commodity;
import team.softarrow.itourism_server.entity.Order;
import team.softarrow.itourism_server.entity.Shop;
import team.softarrow.itourism_server.entity.User;
import team.softarrow.itourism_server.entity.form.BuyCommodityForm;
import team.softarrow.itourism_server.exception.DefinedException;
import team.softarrow.itourism_server.repository.CommodityRepository;
import team.softarrow.itourism_server.repository.OrderRepository;
import team.softarrow.itourism_server.repository.ShopRepository;
import team.softarrow.itourism_server.response.ResponseCode;

import java.util.*;

@Service
public class ShoppingService {
    private ShopRepository shopRepository;
    private CommodityRepository commodityRepository;
    private OrderRepository orderRepository;
    private UserService userService;
    private OrderService orderService;

    @Autowired
    public void setOrderService(OrderService orderService) {
        this.orderService = orderService;
    }

    @Autowired
    public void setShopRepository(ShopRepository shopRepository) {
        this.shopRepository = shopRepository;
    }

    @Autowired
    public void setCommodityRepository(CommodityRepository commodityRepository) {
        this.commodityRepository = commodityRepository;
    }

    @Autowired
    public void setOrderRepository(OrderRepository orderRepository) {
        this.orderRepository = orderRepository;
    }

    @Autowired
    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    /**
     * 根据ID获取商店信息
     * @param id 商店ID
     * @return 商店
     */
    public Shop getShopByID(int id) {
        Optional<Shop> optional = shopRepository.findById(id);
        if (optional.isEmpty()) {
            throw new DefinedException(ResponseCode.NOT_FOUND, "商店不存在");
        }
        return optional.get();
    }

    /**
     * 根据ID获取商品
     * @param id 商品ID
     * @return 商品
     */
    public Commodity getCommodityByID(int id) {
        Optional<Commodity> optional = commodityRepository.findById(id);
        if (optional.isEmpty()) {
            throw new DefinedException(ResponseCode.NOT_FOUND, "商品不存在");
        }
        return optional.get();
    }

    /**
     * 分页获取商店下的商品信息
     * @param id 商店id
     * @param page 页数
     * @param size 每页大小
     * @return 商品信息
     */
    public List<Commodity> getCommoditiesInShop(int id, int page, int size) {
        Shop shop = new Shop();
        shop.setId(id);
        Pageable pageable = PageRequest.of(page, size);
        Page<Commodity> commodities = commodityRepository.findByShop(shop,pageable);
        List<Commodity> commodityList = commodities.getContent();
        for (Commodity commodity : commodityList) {
            String info = commodity.getFigure();
            commodity.setShop(null);
        }

        return commodityList;
    }

    /**
     * 获取商店详细信息
     * @param id 商店ID
     * @return 商店
     */
    public Shop getShopInfo(int id) {
        Shop shop = getShopByID(id);
        String info = shop.getIntroduction();
        info = shop.getFigure();
        shop.setSpot(null);
        return shop;
    }

    /**
     * 获取商品详细信息
     * @param id 商品id
     * @return 商品
     */
    public Commodity getCommodityInfo(int id) {
        Commodity commodity = getCommodityByID(id);
        String info = commodity.getIntroduction();
        info = commodity.getFigure();
        commodity.setShop(null);
        return commodity;
    }

    /**
     * 购买商品
     * @param buyCommodityForm 购买表单
     * @return 支付宝唤起
     * @throws Exception 支付宝请求失败
     */
    public Map<String, String> buyCommodity(BuyCommodityForm buyCommodityForm) throws Exception {
        Commodity commodity = getCommodityByID(buyCommodityForm.getId());

        // 建立订单
        Order order = new Order();
        User user = new User();
        user.setId(userService.getCurrent().getId());
        order.setId(System.currentTimeMillis());
        order.setOrderType(Order.OrderType.Commodity);
        order.setOrderState(Order.OrderState.Pending);
        order.setAmount(buyCommodityForm.getAmount());
        order.setPrice(buyCommodityForm.getAmount() * commodity.getPrice());
        order.setCreateTime(new Date());
        order.setCommodity(commodity);
        order.setUser(user);
        orderRepository.save(order);

        // 向支付宝发起支付
        String body = PaymentUtils.pay(order);
        return Map.of("html", body);
    }

    /**
     * 退货
     * @param id 要退货的订票
     */
    @Transactional
    public void returnCommodity(Long id) throws Exception {
        Order order = orderService.getCanRefundOrder(id);
        if (order.getOrderType() != Order.OrderType.Commodity) {
            throw new DefinedException(ResponseCode.NOT_FOUND, "订单类型错误");
        }

        double price = order.getPrice();
        int amount = order.getAmount();

        // 调起支付宝来退钱
        PaymentUtils.refund(Long.toString(id), Double.toString(price));

        order.setOrderState(Order.OrderState.Refund);
        orderRepository.save(order);
        commodityRepository.decreaseSale(order.getCommodity().getId(), amount);
    }

    /**
     * 完成一个商品订单的交易
     * @param id
     */
    public void finishOrder(Long id) {
        Optional<Order> optional = orderRepository.findById(id);
        if (optional.isEmpty()) {
            throw new DefinedException(ResponseCode.NOT_FOUND, "订单不存在");
        }
        Order order = optional.get();

        if (order.getOrderType() != Order.OrderType.Commodity ||
            order.getOrderState() != Order.OrderState.Transiting) {
            throw new DefinedException(ResponseCode.OPERATE_BUSY, "当前订单不能完成");
        }
        if (!Objects.equals(userService.getCurrent().getId(), order.getUser().getId())) {
            throw new DefinedException(ResponseCode.ACCESS_DENY);
        }

        orderRepository.setState(id, Order.OrderState.Finish.ordinal());
    }
}
