package team.softarrow.itourism_server.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Service;
import team.softarrow.itourism_server.entity.*;
import team.softarrow.itourism_server.exception.DefinedException;
import team.softarrow.itourism_server.repository.CommodityRepository;
import team.softarrow.itourism_server.repository.OrderRepository;
import team.softarrow.itourism_server.repository.ShopRepository;
import team.softarrow.itourism_server.response.ResponseCode;
import team.softarrow.itourism_server.security.VisitorDetail;

import java.util.*;

@Service
public class SellerService {
    private UserService userService;
    private ShoppingService shoppingService;
    private ShopRepository shopRepository;
    private CommodityRepository commodityRepository;
    private PermissionService permissionService;
    private OrderRepository orderRepository;

    @Autowired
    public void setOrderRepository(OrderRepository orderRepository) {
        this.orderRepository = orderRepository;
    }

    @Autowired
    public void setCheckPermissionService(PermissionService permissionService) {
        this.permissionService = permissionService;
    }

    @Autowired
    public void setShopRepository(ShopRepository shopRepository) {
        this.shopRepository = shopRepository;
    }

    @Autowired
    public void setCommodityRepository(CommodityRepository commodityRepository) {
        this.commodityRepository = commodityRepository;
    }

    @Autowired
    public void setShoppingService(ShoppingService shoppingService) {
        this.shoppingService = shoppingService;
    }

    @Autowired
    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    public void checkShop(Shop shop) {
        if (shop.getName() == null) {
            throw new DefinedException(ResponseCode.NOT_MATCH, "店名为空");
        } else if (shop.getFigure() == null) {
            throw new DefinedException(ResponseCode.NOT_MATCH, "图片为空");
        } else if (shop.getIntroduction() == null) {
            throw new DefinedException(ResponseCode.NOT_MATCH, "详细介绍为空");
        } else if (shop.getBrief() == null) {
            throw new DefinedException(ResponseCode.NOT_MATCH, "简介为空");
        }
    }

    public void checkCommodity(Commodity commodity) {
        if (commodity.getName() == null) {
            throw new DefinedException(ResponseCode.NOT_MATCH, "商品名称为空");
        } else if (commodity.getFigure() == null) {
            throw new DefinedException(ResponseCode.NOT_MATCH, "图片为空");
        } else if (commodity.getIntroduction() == null) {
            throw new DefinedException(ResponseCode.NOT_MATCH, "详细介绍为空");
        } else if (commodity.getPrice() == null) {
            throw new DefinedException(ResponseCode.NOT_MATCH, "价格为空");
        }
    }

    public void checkPermission(Shop shop) {
        List<GrantedAuthority> grantedAuthorities = userService.getCurrentPermission();
        VisitorDetail visitorDetail = userService.getCurrent();

        for (GrantedAuthority grantedAuthority : grantedAuthorities) {
            if (grantedAuthority.getAuthority().equals("ROLE_seller")
                && !Objects.equals(visitorDetail.getShopId(), shop.getId())) {
                throw new DefinedException(ResponseCode.ACCESS_DENY);
            }
        }
    }

    /**
     * 增添商店
     * @param shop 商店
     */
    public void addShop(Shop shop) {
        checkShop(shop);
        if (shop.getPosition() == null) {
            throw new DefinedException(ResponseCode.NOT_MATCH, "位置为空");
        }

        Spot spot = new Spot();
        spot.setId(userService.getCurrent().getSpotId());
        shop.setSpot(spot);
        shop.setDeleted(false);
        shopRepository.save(shop);
    }

    /**
     * 编辑商店
     * @param shopForm 商店
     */
    public void editShop(Shop shopForm) {
        if (shopForm.getId() == null) {
            throw new DefinedException(ResponseCode.NOT_MATCH, "ID为空");
        }
        checkPermission(shopForm);
        Shop shop = shoppingService.getShopByID(shopForm.getId());

        Optional.ofNullable(shopForm.getName())
                .ifPresent(shop::setName);
        Optional.ofNullable(shopForm.getFigure())
                .ifPresent(shop::setFigure);
        Optional.ofNullable(shopForm.getIntroduction())
                .ifPresent(shop::setIntroduction);
        Optional.ofNullable(shopForm.getBrief())
                .ifPresent(shop::setBrief);

        shopRepository.save(shop);
    }

    /**
     * 删除商店
     * @param id 商店id
     */
    public void deleteShop(int id) {
        Shop shop = shoppingService.getShopByID(id);
        permissionService.checkSpotPermission(shop.getSpot().getId());

        // 删除商店内所有商品
        Set<Commodity> commodities = shop.getCommodities();
        for (Commodity commodity : commodities) {
            commodity.setDeleted(true);
        }

        // 剥夺该店商家的权限
        Set<User> sellers = shop.getSellers();
        Iterator<User> iterator = sellers.iterator();
        while (iterator.hasNext()) {
            User seller = iterator.next();
            permissionService.deprive(seller);
            iterator.remove();
            seller.setShop(null);
        }

        shop.setDeleted(true);
        shopRepository.save(shop);
    }

    /**
     * 上架商品
     * @param commodity
     */
    public void addCommodity(Commodity commodity) {
        checkCommodity(commodity);

        Shop shop = new Shop();
        shop.setId(userService.getCurrent().getShopId());
        commodity.setShop(shop);
        commodity.setSales(0L);
        commodity.setDeleted(false);
        commodityRepository.save(commodity);
    }

    /**
     * 编辑商品
     * @param commodityForm 商品
     */
    public void editCommodity(Commodity commodityForm) {
        if (commodityForm.getId() == null) {
            throw new DefinedException(ResponseCode.NOT_MATCH, "ID为空");
        }
        Commodity commodity = shoppingService.getCommodityByID(commodityForm.getId());
        checkPermission(commodity.getShop());

        Optional.ofNullable(commodityForm.getName())
                .ifPresent(commodity::setName);
        Optional.ofNullable(commodityForm.getFigure())
                .ifPresent(commodity::setFigure);
        Optional.ofNullable(commodityForm.getIntroduction())
                .ifPresent(commodity::setIntroduction);
        Optional.ofNullable(commodityForm.getPrice())
                .ifPresent(commodity::setPrice);

        commodityRepository.save(commodity);
    }

    /**
     * 删除商品
     * @param id 商品id
     */
    public void deleteCommodity(Integer id) {
        Commodity commodity = shoppingService.getCommodityByID(id);
        checkPermission(commodity.getShop());
        commodityRepository.deleteCommodity(id);
    }

    /**
     * 商品发货
     * @param id 订单id
     */
    public void deliverCommodity(Long id) {
        Optional<Order> optional = orderRepository.findById(id);
        if (optional.isEmpty()) {
            throw new DefinedException(ResponseCode.NOT_FOUND, "订单不存在");
        }
        Order order = optional.get();
        if (order.getOrderType() != Order.OrderType.Commodity) {
            throw new DefinedException(ResponseCode.OPERATE_BUSY, "当前订单不能发货");
        }
        if (!Objects.equals(userService.getCurrent().getShopId(), order.getCommodity().getShop().getId())) {
            throw new DefinedException(ResponseCode.ACCESS_DENY);
        }

        orderRepository.setState(id, Order.OrderState.Transiting.ordinal());
    }

    /**
     * 获取自己管理店铺下的所有订单
     * @param page
     * @param size
     * @return
     */
    public List<Order> getOrdersInShop(Integer page, Integer size) {
        int shop_id = userService.getCurrent().getShopId();
        Shop shop = new Shop();
        shop.setId(shop_id);

        Pageable pageable = PageRequest.of(page, size);
        Page<Order> orders = orderRepository.findShopOrders(shop, pageable);
        List<Order> orderList = orders.getContent();
        for (Order order : orderList) {
            Commodity commodity = order.getCommodity();
            commodity.setShop(null);
        }
        return orderList;
    }
}
