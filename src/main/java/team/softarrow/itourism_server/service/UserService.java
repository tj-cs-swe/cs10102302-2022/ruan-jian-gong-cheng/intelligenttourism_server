package team.softarrow.itourism_server.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import team.softarrow.itourism_server.entity.*;
import team.softarrow.itourism_server.exception.DefinedException;
import team.softarrow.itourism_server.repository.*;
import team.softarrow.itourism_server.response.ResponseCode;
import team.softarrow.itourism_server.security.VisitorDetail;

import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
public class UserService {

    private BCryptPasswordEncoder passwordEncoder;
    private UserRepository userRepository;
    private OrderRepository orderRepository;
    private TicketRepository ticketRepository;
    private ShopRepository shopRepository;
    private SpotRepository spotRepository;
    private PermissionRepository permissionRepository;
    private RedisService redisService;

    @Autowired
    public void setShopRepository(ShopRepository shopRepository) {
        this.shopRepository = shopRepository;
    }

    @Autowired
    public void setSpotRepository(SpotRepository spotRepository) {
        this.spotRepository = spotRepository;
    }

    @Autowired
    public void setPermissionRepository(PermissionRepository permissionRepository) {
        this.permissionRepository = permissionRepository;
    }

    @Autowired
    public void setRedisService(RedisService redisService) {
        this.redisService = redisService;
    }

    @Autowired
    public void setVisitorRepository(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Autowired
    public void setPasswordEncoder(BCryptPasswordEncoder passwordEncoder) {
        this.passwordEncoder = passwordEncoder;
    }

    @Autowired
    public void setOrderRepository(OrderRepository orderRepository) {
        this.orderRepository = orderRepository;
    }

    @Autowired
    public void setTicketRepository(TicketRepository ticketRepository) {
        this.ticketRepository = ticketRepository;
    }

    /**
     * 查询用户信息：根据id
     * @param id
     */
    public User getUserById(Integer id){
        Optional<User> visitor = userRepository.findById(id);
        if (visitor.isPresent()) {
            return visitor.get();
        }
        throw new DefinedException(ResponseCode.NOT_FOUND);
    }

    /**
     * 分页获取用户列表
     * @param page 页码
     * @param size 页面大小
     * @return 用户列表
     */
    public List<User> getUserList(Integer page, Integer size) {
        Pageable pageable = PageRequest.of(page, size);
        List<User> users = userRepository.findAllUser(pageable).getContent();
        for (User user : users) {
            user.setShop(null);
            user.setAdminSpot(null);
        }
        return users;
    }

    /**
     * 更改用户权限
     * @param info 权限更改信息
     */
    public void changeUserPermission(Map<String, Integer> info) {
        User user = getUserById(info.get("users_id"));
        int perm_id = info.get("permissions_id");
        Optional<Permission> optional = permissionRepository.findById(perm_id);
        if (optional.isEmpty()) {
            throw new DefinedException(ResponseCode.NOT_MATCH, "权限不存在");
        }

        Permission permission = optional.get();
        user.getPermissions().clear();
        user.getPermissions().add(permission);
        user.setShop(null);
        user.setAdminSpot(null);

        // 如果是景区管理员或商家，还需要分配景区和商店
        Integer manage_id = info.get("manage_id");
        if (permission.getName().equals("ROLE_spot_admin")) {
            if (manage_id == null || !spotRepository.existsById(manage_id)) {
                throw new DefinedException(ResponseCode.NOT_MATCH, "分配的景区不存在");
            }
            Spot spot = new Spot();
            spot.setId(manage_id);
            user.setAdminSpot(spot);
        } else if (permission.getName().equals("ROLE_seller")) {
            if (manage_id == null || !shopRepository.existsById(manage_id)) {
                throw new DefinedException(ResponseCode.NOT_MATCH, "分配的商店不存在");
            }
            Shop shop = new Shop();
            shop.setId(manage_id);
            user.setShop(shop);
        }

        userRepository.save(user);
        redisService.delete(user.getUsername());
    }

    /**
     * 修改密码
     * @param info 信息
     */
    public void changePassword(Map<String, String> info) {
        if (!info.get("new_password").equals(info.get("check_password"))) {
            throw new DefinedException(ResponseCode.NOT_MATCH, "两次密码不相等");
        }

        User user = getCurrentUser();
        if (!passwordEncoder.matches(info.get("old_password"), user.getPassword())) {
            throw new DefinedException(ResponseCode.NOT_MATCH, "密码错误");
        }
        user.setPassword(passwordEncoder.encode(info.get("new_password")));
        userRepository.save(user);
    }

    /**
     * 修改个人信息
     * @param change 改动
     * 检测：认为性别只有男女，年龄不能为负
     */
    public User edit(Map<String,String> change) {
        String age = change.get("age");
        String gender = change.get("gender");
        String avatar = change.get("avatar");
        String phone = change.get("phone");
        if (age.isEmpty() || gender.isEmpty()) {
            throw new DefinedException(ResponseCode.CONTENT_EMPTY,"修改项不得为空");
        }
        if (!(gender.equals("女") || gender.equals("男") || gender.equals("保密"))) {
            throw new DefinedException(ResponseCode.NOT_MATCH, "性别错误");
        }
        if (Integer.parseInt(age) < 0 || Integer.parseInt(age) > 150) {
            throw new DefinedException(ResponseCode.NOT_MATCH, "年龄错误");
        }

        User user = getCurrentUser();
        user.setAge(Integer.valueOf(age));
        user.setGender(gender);
        Optional.ofNullable(avatar).ifPresent(user::setAvatar);
        Optional.ofNullable(phone).ifPresent(user::setPhone);
        userRepository.save(user);
        return user;
    }

    /**
     * 头像base64存储
     * @param data
     */
    public void uploadAvatar(String data) {
        VisitorDetail detail = getCurrent();
        userRepository.updateAvatar(detail.getId(), data);
    }

    /**
     * 获取当前连接的用户
     * @return User
     */
    public User getCurrentUser() {
        VisitorDetail detail = getCurrent();
        Optional<User> optional = userRepository.findById(detail.getId());
        if (optional.isEmpty()) {
            throw new DefinedException(ResponseCode.NOT_FOUND, "用户不存在");
        }
        return optional.get();
    }

    /**
     * 获取当前用户的权限
     * @return 权限
     */
    public List<GrantedAuthority> getCurrentPermission() {
        VisitorDetail detail = getCurrent();
        return detail.getAuthorities();
    }

    /**
     * 获取当前连接的用户简要信息
     * @return 简要信息
     */
    public VisitorDetail getCurrent() {
        VisitorDetail detail = new VisitorDetail();
        detail.setId(-1);
        Object object = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (object instanceof VisitorDetail) {
            return (VisitorDetail) object;
        }
        return detail;
    }

    /**
     * 获取当前连接用户的详细信息
     * @return User
     */
    public User getCurrentUserInfo() {
        User user = getCurrentUser();
        user.getPermissions().size();
        return user;
    }

    /**
     * 获取当前用户的所有订单
     * @param page 页数
     * @param size 每页显示的数量
     * @return 订单集合
     */
    public List<Order> getCommodityOrderBelongMe(int page, int size) {
        User user = new User();
        user.setId(getCurrent().getId());
        Pageable pageable = PageRequest.of(page, size);
        Page<Order> orders = orderRepository.findCommodityOrders(user, pageable);
        for (Order order : orders) {
            order.getCommodity().setShop(null);
        }
        return orders.getContent();
    }

    /**
     * 获取当前用户的所有订单
     * @param page 页数
     * @param size 每页显示的数量
     * @return 订单集合
     */
    public List<Order> getTicketOrderBelongMe(int page, int size) {
        User user = new User();
        user.setId(getCurrent().getId());
        Pageable pageable = PageRequest.of(page, size);
        Page<Order> orders = orderRepository.findTicketOrders(user, pageable);
        return orders.getContent();
    }

    /**
     * 获取当前用户的所有票
     * @param page 页数
     * @param size 每页显示的数量
     * @return 票集合
     */
    public List<Ticket> getTicketBelongMe(int page, int size) {
        User user = new User();
        user.setId(getCurrent().getId());
        Pageable pageable = PageRequest.of(page, size);
        Page<Ticket> tickets = ticketRepository.findByUser(user, pageable);
        return tickets.getContent();
    }
}
