package team.softarrow.itourism_server.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import team.softarrow.itourism_server.entity.Order;
import team.softarrow.itourism_server.exception.DefinedException;
import team.softarrow.itourism_server.repository.OrderRepository;
import team.softarrow.itourism_server.response.ResponseCode;

import java.util.Optional;

@Service
public class OrderService {
    private UserService userService;
    private OrderRepository orderRepository;

    @Autowired
    public void setOrderRepository(OrderRepository orderRepository) {
        this.orderRepository = orderRepository;
    }

    @Autowired
    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    public Order getCanRefundOrder(Long id) {
        Optional<Order> optional = orderRepository.findById(id);
        if (optional.isEmpty()) {
            throw new DefinedException(ResponseCode.NOT_FOUND,"订单不存在");
        }
        Order order = optional.get();
        Order.OrderState state = order.getOrderState();
        if (state != Order.OrderState.Paid && state != Order.OrderState.Transiting) {
            throw new DefinedException(ResponseCode.CONDITION_NOT_MET);
        }
        if (!order.getUser().getId().equals(userService.getCurrent().getId())) {
            throw new DefinedException(ResponseCode.AUTHORITY_DENY);
        }
        return order;
    }
}
