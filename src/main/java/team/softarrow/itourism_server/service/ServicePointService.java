package team.softarrow.itourism_server.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import team.softarrow.itourism_server.entity.Comment;
import team.softarrow.itourism_server.entity.IntroAudio;
import team.softarrow.itourism_server.entity.ServicePoint;
import team.softarrow.itourism_server.entity.Spot;
import team.softarrow.itourism_server.exception.DefinedException;
import team.softarrow.itourism_server.repository.ServicePointRepository;
import team.softarrow.itourism_server.response.ResponseCode;

import java.util.List;
import java.util.Optional;
import java.util.Set;

@Service
public class ServicePointService {
    private UserService userService;
    private ServicePointRepository servicePointRepository;
    private PermissionService permissionService;

    @Autowired
    public void setCheckPermissionService(PermissionService permissionService) {
        this.permissionService = permissionService;
    }

    @Autowired
    public void setServicePointRepository(ServicePointRepository servicePointRepository) {
        this.servicePointRepository = servicePointRepository;
    }

    @Autowired
    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    public void checkServicePoint(ServicePoint servicePoint) {
        if (servicePoint.getName() == null) {
            throw new DefinedException(ResponseCode.NOT_MATCH, "景点为空");
        } else if (servicePoint.getType() == null) {
            throw new DefinedException(ResponseCode.NOT_MATCH, "类型为空");
        } else if (servicePoint.getFigure() == null) {
            throw new DefinedException(ResponseCode.NOT_MATCH, "图片为空");
        } else if (servicePoint.getIntroduce() == null) {
            throw new DefinedException(ResponseCode.NOT_MATCH, "详细介绍为空");
        } else if (servicePoint.getPosition() == null) {
            throw new DefinedException(ResponseCode.NOT_MATCH, "位置为空");
        }
    }

    /**
     * 增添景点
     * @param servicePoint 景点
     */
    public void addServicePoint(ServicePoint servicePoint) {
        checkServicePoint(servicePoint);

        Spot spot = new Spot();
        spot.setId(userService.getCurrent().getSpotId());
        servicePoint.setDeleted(false);
        servicePoint.setSpot(spot);
        servicePointRepository.save(servicePoint);
    }

    /**
     * 编辑景点
     * @param servicePointForm 景点
     */
    public void editServicePoint(ServicePoint servicePointForm) {
        if (servicePointForm.getId() == null) {
            throw new DefinedException(ResponseCode.NOT_MATCH, "id为空");
        }
        ServicePoint servicePoint = getServicePointByID(servicePointForm.getId());
        permissionService.checkSpotPermission(servicePoint.getSpot().getId());

        Optional.ofNullable(servicePointForm.getName())
                .ifPresent(servicePoint::setName);
        Optional.ofNullable(servicePointForm.getType())
                .ifPresent(servicePoint::setType);
        Optional.ofNullable(servicePointForm.getPosition())
                .ifPresent(servicePoint::setPosition);
        Optional.ofNullable(servicePointForm.getFigure())
                .ifPresent(servicePoint::setFigure);
        Optional.ofNullable(servicePointForm.getIntroduce())
                .ifPresent(servicePoint::setIntroduce);
        servicePointRepository.save(servicePoint);
    }

    /**
     * 根据id获取景点信息
     * @param id 景点id
     * @return 景点
     */
    public ServicePoint getServicePointByID(Integer id) {
        Optional<ServicePoint> optional = servicePointRepository.findById(id);
        if (optional.isEmpty()) {
            throw new DefinedException(ResponseCode.NOT_FOUND, "景点不存在");
        }
        return optional.get();
    }

    /**
     * 删除景点
     * @param id 景点id
     */
    public void deleteServicePoint(Integer id) {
        ServicePoint servicePoint = getServicePointByID(id);
        permissionService.checkSpotPermission(servicePoint.getSpot().getId());

        // 删除景点，包括其下面的音频、评论
        servicePoint.setDeleted(true);
        Set<IntroAudio> introAudios = servicePoint.getIntroAudios();
        for (IntroAudio introAudio : introAudios) {
            introAudio.setDeleted(true);
        }
        Set<Comment> comments = servicePoint.getComments();
        for (Comment comment : comments) {
            comment.setDeleted(true);
        }
        servicePointRepository.save(servicePoint);
    }

    /**
     * 获取景点详情
     * @param id 景点id
     * @return 景点
     */
    public ServicePoint getServicePointInfo(Integer id) {
        ServicePoint servicePoint = getServicePointByID(id);
        servicePoint.getFigure();
        servicePoint.getIntroduce();
        return servicePoint;
    }

    /**
     * 分页获取景区下的景点
     * @param spot_id 景区id
     * @param page 页码
     * @param size 页面大小
     * @return 景点列表
     */
    public  List<ServicePoint> getServicePointList(Integer spot_id, Integer page, Integer size) {
        Spot spot = new Spot();
        spot.setId(spot_id);

        Pageable pageable = PageRequest.of(page, size);
        List<ServicePoint> servicePoints = servicePointRepository.findBySpot(spot, pageable).getContent();
        for (ServicePoint servicePoint : servicePoints) {
            servicePoint.getFigure();
            servicePoint.setSpot(null);
        }
        return servicePoints;
    }

    /**
     * 获取景区下的所有景点
     * @param spot_id 景区id
     * @return 景点列表
     */
    public List<ServicePoint> getAllServicePoint(Integer spot_id) {
        Spot spot = new Spot();
        spot.setId(spot_id);
        List<ServicePoint> servicePoints = servicePointRepository.findAllBySpot(spot);
        for (ServicePoint servicePoint : servicePoints) {
            servicePoint.setSpot(null);
        }
        return servicePoints;
    }
}
