package team.softarrow.itourism_server.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import team.softarrow.itourism_server.common.utils.PaymentUtils;
import team.softarrow.itourism_server.entity.*;
import team.softarrow.itourism_server.entity.form.BuyTicketForm;
import team.softarrow.itourism_server.exception.DefinedException;
import team.softarrow.itourism_server.repository.ShopRepository;
import team.softarrow.itourism_server.repository.SpotRepository;
import team.softarrow.itourism_server.repository.TicketRepository;
import team.softarrow.itourism_server.response.ResponseCode;
import team.softarrow.itourism_server.security.VisitorDetail;

import java.util.*;

@Service
public class SpotService {
    private SpotRepository spotRepository;
    private ShopRepository shopRepository;
    private PermissionService permissionService;
    private TicketRepository ticketRepository;
    private UserService userService;
    private OrderService orderService;

    @Autowired
    public void setPermissionService(PermissionService permissionService) {
        this.permissionService = permissionService;
    }

    @Autowired
    public void setOrderService(OrderService orderService) {
        this.orderService = orderService;
    }

    @Autowired
    public void setSpotRepository(SpotRepository spotRepository) {
        this.spotRepository = spotRepository;
    }

    @Autowired
    public void setTicketRepository(TicketRepository ticketRepository) {
        this.ticketRepository = ticketRepository;
    }

    @Autowired
    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    @Autowired
    public void setShopRepository(ShopRepository shopRepository) {
        this.shopRepository = shopRepository;
    }

    /**
     * 获取景区对象
     * @param id 景区id
     * @return Spot
     */
    public Spot getSpot(int id) {
        Optional<Spot> optional = spotRepository.findById(id);
        if (optional.isEmpty()) {
            throw new DefinedException(ResponseCode.NOT_FOUND, "景区不存在");
        }
        return optional.get();
    }

    /**
     * 获取景区详细信息
     * @param id 景区id
     * @return 景区
     */
    public Spot getSpotInfo(int id) {
        Spot spot = getSpot(id);
        spot.getFigure();
        spot.getIntroduction();

        Set<User> users = spot.getEvaluate();
        User me = new User();
        me.setId(userService.getCurrent().getId());
        spot.setIfScore(users.contains(me));
        spot.setExistScore(!users.isEmpty());
        spot.setEvaluate(null);

        return spot;
    }

    /**
     * 获取景区内的所有商店
     * @param sid 景区id
     * @return 商店集合
     */
    public List<Shop> getShopsInSpot(int sid, int page, int size) {
        Spot spot = new Spot();
        spot.setId(sid);
        spot.setDeleted(false);

        Pageable pageable = PageRequest.of(page, size);
        Page<Shop> shops = shopRepository.findBySpot(spot, pageable);
        List<Shop> shopList = shops.getContent();
        for (Shop shop : shopList) {
            shop.setSpot(null);
            shop.getFigure();
        }

        return shopList;
    }

    /**
     * 开始购票
     * @param buyTicketForm 购票表单
     * @return 订单信息
     * @throws Exception 提交发生错误
     */
    public Map<String, String> buyTicket(BuyTicketForm buyTicketForm) throws Exception {
        Spot spot = getSpot(buyTicketForm.getSpotId());
        int need_amount = buyTicketForm.getIdCard().size();
        if (need_amount != buyTicketForm.getAmount()) {
            throw new DefinedException(ResponseCode.OPERATE_BUSY, "数量错误");
        }
        // 检查余票
        if (spot.getLeftTickets() < need_amount) {
            throw new DefinedException(ResponseCode.OPERATE_BUSY, "余票不足");
        }

        // 建立订单
        Order order = new Order();
        User user = new User();
        user.setId(userService.getCurrent().getId());
        order.setId(System.currentTimeMillis());
        order.setOrderType(Order.OrderType.Ticket);
        order.setOrderState(Order.OrderState.Pending);
        order.setAmount(buyTicketForm.getAmount());
        order.setPrice(buyTicketForm.getAmount() * spot.getTicketPrice());
        order.setCreateTime(new Date());
        order.setUser(user);
        order.setUser(user);
        order.setSpot(spot);

        // 建立票
        Set<Ticket> tickets = new HashSet<>();
        for (String idCard : buyTicketForm.getIdCard()) {
            Ticket ticket = new Ticket();
            ticket.setSpot(spot);
            ticket.setUser(user);
            ticket.setTicketState(Ticket.TicketState.Pending);
            ticket.setIdCard(idCard);
            ticket.setUseTime(buyTicketForm.getUseTime());
            ticket.setBuyTime(new Date());
            ticket.setPrice(spot.getTicketPrice());
            ticket.setOrder(order);
            tickets.add(ticket);
        }
        ticketRepository.saveAll(tickets);

        // 向支付宝发起支付
        String body = PaymentUtils.pay(order);
        return Map.of("html", body);
    }

    /**
     * 用户退票
     */
    @Transactional
    public void returnTicket(Long id) throws Exception {
        Order order = orderService.getCanRefundOrder(id);
        if (order.getOrderType() != Order.OrderType.Ticket) {
            throw new DefinedException(ResponseCode.NOT_FOUND, "订单类型错误");
        }

        Set<Ticket> tickets = order.getTickets();
        double price = order.getPrice();
        int refund_amount = tickets.size();
        int spot_id = -1;

        // 修改票的状态
        order.setOrderState(Order.OrderState.Refund);
        for (Ticket ticket : tickets) {
            if (spot_id == -1) spot_id = ticket.getSpot().getId();
            if (ticket.getTicketState() == Ticket.TicketState.Finish) {
                price -= ticket.getPrice();
                refund_amount -= 1;
                continue;
            }
            if (ticket.getUseTime().before(new Date())) {
                throw new DefinedException(ResponseCode.CONDITION_NOT_MET, "超时无法退票");
            }
            ticket.setTicketState(Ticket.TicketState.Refund);
        }

        // 调起支付宝来退钱
        PaymentUtils.refund(Long.toString(id), Double.toString(price));

        //退票后状态变更：票状态；余票；钱；
        ticketRepository.saveAll(List.copyOf(tickets));
        spotRepository.increaseTicket(spot_id, refund_amount);
    }

    /**
     * 增加景区
     * 管理员
     */
    public void addSpot(Spot spotForm) {
        if (spotForm.getName() == null) {
            throw new DefinedException(ResponseCode.NOT_MATCH, "名字为空");
        } else if (spotForm.getIntroduction() == null) {
            throw new DefinedException(ResponseCode.NOT_MATCH, "介绍为空");
        } else if (spotForm.getFigure() == null) {
            throw new DefinedException(ResponseCode.NOT_MATCH, "图片为空");
        } else if (spotForm.getTicketPrice() == null) {
            throw new DefinedException(ResponseCode.NOT_MATCH, "票价为空");
        } else if (spotForm.getLeftTickets() == null) {
            throw new DefinedException(ResponseCode.NOT_MATCH, "余票为空");
        } else if (spotForm.getPosition() == null) {
            throw new DefinedException(ResponseCode.NOT_MATCH, "坐标为空");
        }
        spotForm.setDeleted(false);
        spotForm.setScore(5.0);
        spotRepository.save(spotForm);
    }

    /**
     * 修改景区信息
     * 管理员+景区管理员
     */
    public void editSpot(Spot spotForm) {
        if (spotForm.getId() == null) {
            throw new DefinedException(ResponseCode.NOT_MATCH, "ID为空");
        }
        Spot spot = getSpot(spotForm.getId());

        // 如果是景区管理员，只能管理自己的景区
        List<GrantedAuthority> grantedAuthorities = userService.getCurrentPermission();
        VisitorDetail visitorDetail = userService.getCurrent();
        for (GrantedAuthority grantedAuthority : grantedAuthorities) {
            if (grantedAuthority.getAuthority().equals("ROLE_spot_admin")
                    && !Objects.equals(visitorDetail.getSpotId(), spot.getId())) {
                throw new DefinedException(ResponseCode.AUTHORITY_DENY);
            }
        }

        Optional.ofNullable(spotForm.getName())
                .ifPresent(spot::setName);
        Optional.ofNullable(spotForm.getFigure())
                .ifPresent(spot::setFigure);
        Optional.ofNullable(spotForm.getIntroduction())
                .ifPresent(spot::setIntroduction);
        Optional.ofNullable(spotForm.getLeftTickets())
                .ifPresent(spot::setLeftTickets);
        Optional.ofNullable(spotForm.getTicketPrice())
                .ifPresent(spot::setTicketPrice);
        Optional.ofNullable(spotForm.getPosition())
                .ifPresent(spot::setPosition);
        spotForm.setDeleted(false);
        spotRepository.save(spot);
    }

    /**
     * 删除景区
     * 管理员
     */
    public void deleteSpot(Integer id) {
        Spot spot = spotRepository.findSpotById(id).orElse(null);
        if (spot == null) {
            throw new DefinedException(ResponseCode.NOT_MATCH, "景区不存在");
        }

        // 删除景区下的所有商店、商品
        spot.setDeleted(true);
        Set<Shop> shops = spot.getShops();
        for (Shop shop : shops) {
            shop.setDeleted(true);
        }

        // 删除景区下的所有景点
        Set<ServicePoint> servicePoints = spot.getServicePoints();
        for (ServicePoint servicePoint : servicePoints) {
            servicePoint.setDeleted(true);
        }

        // 删除景区下所有的游览路线
        Set<TourRoute> tourRoutes = spot.getTourRoutes();
        for (TourRoute tourRoute : tourRoutes) {
            tourRoute.setDeleted(true);
        }

        // 剥夺所有管理景区的用户的权限
        Set<User> admins = spot.getSpotAdmin();
        Iterator<User> iterator = admins.iterator();
        while (iterator.hasNext()) {
            User admin = iterator.next();
            permissionService.deprive(admin);
            iterator.remove();
            admin.setAdminSpot(null);
        }
        spotRepository.save(spot);
    }

    /**
     * 查询景区
     */
    public Spot findByName(String name) {
        Optional<Spot> spot = spotRepository.findByName(name);
        if (spot.isPresent()) {
            return spot.get();
        }
        throw new DefinedException(ResponseCode.NOT_FOUND);
    }

    public List<Spot> getSpotList(Integer page, Integer size) {
        Pageable pageable = PageRequest.of(page, size);
        return spotRepository.findAll(pageable).getContent();
    }

    /**
     * 评价景区
     * @param id 景区id
     * @param score 分值
     */
    public void scoreSpot(Integer id, Integer score) {
        if (score < 0 || score > 5) {
            throw new DefinedException(ResponseCode.NOT_MATCH, "非法分值");
        }

        Spot spot = getSpot(id);
        Set<User> users = spot.getEvaluate();
        User me = new User();
        me.setId(userService.getCurrent().getId());
        if (users.contains(me)) {
            throw new DefinedException(ResponseCode.NOT_MATCH, "已经评分过了");
        }
        double spot_score = spot.getScore() * users.size() + score;
        spot.setScore(spot_score /  (users.size() + 1));
        users.add(me);

        spotRepository.save(spot);
    }

}
