package team.softarrow.itourism_server.service;

import com.google.code.kaptcha.impl.DefaultKaptcha;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import team.softarrow.itourism_server.common.captcha.ImageCode;
import team.softarrow.itourism_server.common.utils.JwtUtil;
import team.softarrow.itourism_server.entity.Permission;
import team.softarrow.itourism_server.entity.User;
import team.softarrow.itourism_server.exception.DefinedException;
import team.softarrow.itourism_server.repository.PermissionRepository;
import team.softarrow.itourism_server.repository.UserRepository;
import team.softarrow.itourism_server.response.ResponseCode;
import team.softarrow.itourism_server.security.VisitorDetail;

import javax.servlet.http.HttpServletRequest;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.*;
import java.util.concurrent.TimeUnit;

import static team.softarrow.itourism_server.response.ResponseCode.CHECK_ERROR;
import static team.softarrow.itourism_server.response.ResponseCode.EMAIL_TOO_OFTEN;

@Service
public class AuthService {
    @Value("${spring.mail.username}")
    private String mailFrom;
    private UserRepository userRepository;
    private DefaultKaptcha captchaProducer;
    private BCryptPasswordEncoder passwordEncoder;
    private RedisService redisService;
    private JavaMailSender mailSender;
    private PermissionRepository permissionRepository;

    @Autowired
    public void setPermissionRepository(PermissionRepository permissionRepository) {
        this.permissionRepository = permissionRepository;
    }

    @Autowired
    public void setCaptchaProducer(DefaultKaptcha defaultKaptcha) {
        this.captchaProducer = defaultKaptcha;
    }

    @Autowired
    public void setVisitorRepository(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Autowired
    public void setPasswordEncoder(BCryptPasswordEncoder passwordEncoder) {
        this.passwordEncoder = passwordEncoder;
    }

    @Autowired
    public void setRedisService(RedisService redisService) {
        this.redisService = redisService;
    }

    @Autowired
    public void setMailSender(JavaMailSender mailSender) {
        this.mailSender = mailSender;
    }

    /**
     * 生成图片验证码
     * @return 图片验证码
     */
    public ImageCode createImageCode() throws IOException {
        ImageCode captcha = new ImageCode();
        String text = captchaProducer.createText();
        BufferedImage image = captchaProducer.createImage(text);
        captcha.setUuid(UUID.randomUUID());
        captcha.setImage(image);
        captcha.setText(text);
        captcha.setExpireTime(LocalDateTime.now().plusMinutes(5));

        // 将验证码存入到Redis中方便后续比对
        redisService.set(captcha.getUuid().toString(), captcha.getText(), 5, TimeUnit.MINUTES);

        return captcha;
    }

    /**
     * 向邮箱发送验证码
     * @param email 邮箱地址
     */
    public void sendEmail (String email) {
        String text = captchaProducer.createText();
        text += captchaProducer.createText();

        String value = redisService.get(email);
        // 若有以前发送过的记录，检查一下是否发送频繁
        if (value != null) {
            long expire = redisService.getExpire(email);
            if (expire >= 240) {
                throw new DefinedException(EMAIL_TOO_OFTEN);
            }
        }

        // 将发送邮件的信息存入redis
        redisService.set(email, text, 5, TimeUnit.MINUTES);

        // 发送验证码邮件
        SimpleMailMessage mail = new SimpleMailMessage();
        mail.setTo(email);
        mail.setSubject("验证码");
        mail.setText(text);
        mail.setFrom(mailFrom);
        mailSender.send(mail);
    }

    /**
     * 检查邮箱验证码
     * @param visitorDetail 验证码
     */
    public void checkEmailCaptcha(VisitorDetail visitorDetail) {
        // 校验邮箱中的验证码
        String value = redisService.get(visitorDetail.getEmail());
        if (value == null) {
            throw new DefinedException(ResponseCode.CHECK_ERROR);
        }
        if (!value.equals(visitorDetail.getCaptcha())) {
            throw new DefinedException(CHECK_ERROR);
        }
        redisService.delete(visitorDetail.getEmail());
    }

    /**
     * 注册用户
     * @param request http请求
     * @param visitorDetail 用户
     * @return Token
     */
    public Map<String, String> registerUser(HttpServletRequest request, VisitorDetail visitorDetail) {
        if (visitorDetail.getUsername() == null) {
            throw new DefinedException(ResponseCode.REGISTER_FAIL, "用户名为空");
        }
        if (visitorDetail.getEmail() == null) {
            throw new DefinedException(ResponseCode.REGISTER_FAIL, "邮箱为空");
        }
        if (visitorDetail.getPassword() == null) {
            throw new DefinedException(ResponseCode.REGISTER_FAIL, "密码为空");
        }
        if (visitorDetail.getCheckPassword() == null) {
            throw new DefinedException(ResponseCode.REGISTER_FAIL, "确认密码为空");
        }
        if (!visitorDetail.getPassword().equals(visitorDetail.getCheckPassword())) {
            throw new DefinedException(ResponseCode.REGISTER_FAIL, "两次密码不相等");
        }
        if (visitorDetail.getCaptcha() == null) {
            throw new DefinedException(ResponseCode.CHECK_ERROR);
        }
        if (userRepository.findVisitorByUsername(visitorDetail.getUsername()).isPresent()) {
            throw new DefinedException(ResponseCode.REGISTER_FAIL, "该用户名已存在");
        }
        if (userRepository.getByEmail(visitorDetail.getEmail()).isPresent()) {
            throw new DefinedException(ResponseCode.REGISTER_FAIL, "该邮箱已被注册");
        }

        // 校验邮箱验证码
        checkEmailCaptcha(visitorDetail);

        // 通过校验，开始新建用户
        User user = new User();
        Permission permission = permissionRepository.findById(1).orElse(new Permission());
        user.setUsername(visitorDetail.getUsername());
        user.setPassword(passwordEncoder.encode(visitorDetail.getPassword()));
        user.setEmail(visitorDetail.getEmail());
        user.setIp(request.getRemoteAddr());
        user.setPermissions(new HashSet<>(Set.of(permission)));
        user.setAvatar("34d34aca-2b79-4223-9182-f972783f57a3.jpg");
        userRepository.save(user);

        visitorDetail.setAuthoritiesFromString("ROLE_user");
        String token = JwtUtil.createToken(visitorDetail);
        redisService.set(visitorDetail.getUsername(), token);
        return Map.of("token", token);
    }

    public void forgetPassword(VisitorDetail visitorDetail) {
        if (visitorDetail.getEmail() == null) {
            throw new DefinedException(ResponseCode.NOT_MATCH, "邮箱为空");
        }
        if (visitorDetail.getPassword() == null) {
            throw new DefinedException(ResponseCode.NOT_MATCH, "密码为空");
        }
        if (visitorDetail.getCaptcha() == null) {
            throw new DefinedException(ResponseCode.CHECK_ERROR);
        }
        Optional<User> optional = userRepository.getByEmail(visitorDetail.getEmail());
        if (optional.isEmpty()) {
            throw new DefinedException(ResponseCode.NOT_EXIST, "不存在此账号");
        }
        checkEmailCaptcha(visitorDetail);

        // 通过校验，修改密码
        User user = optional.get();
        user.setPassword(passwordEncoder.encode(visitorDetail.getPassword()));
        userRepository.save(user);
        redisService.delete(user.getUsername());
    }

}
