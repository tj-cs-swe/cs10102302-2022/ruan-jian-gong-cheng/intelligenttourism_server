package team.softarrow.itourism_server.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Service;
import team.softarrow.itourism_server.entity.Permission;
import team.softarrow.itourism_server.entity.User;
import team.softarrow.itourism_server.exception.DefinedException;
import team.softarrow.itourism_server.response.ResponseCode;
import team.softarrow.itourism_server.security.VisitorDetail;

import java.util.List;
import java.util.Objects;

@Service
public class PermissionService {

    private UserService userService;
    private RedisService redisService;

    @Autowired
    public void setRedisService(RedisService redisService) {
        this.redisService = redisService;
    }

    @Autowired
    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    /**
     * 检查这个用户是否有管理该景区的权限
     * @param id 景区id
     */
    public void checkSpotPermission(int id) {
        List<GrantedAuthority> grantedAuthorities = userService.getCurrentPermission();
        VisitorDetail visitorDetail = userService.getCurrent();

        for (GrantedAuthority grantedAuthority : grantedAuthorities) {
            if (grantedAuthority.getAuthority().equals("ROLE_spot_admin")
                    && !Objects.equals(visitorDetail.getSpotId(), id)) {
                throw new DefinedException(ResponseCode.ACCESS_DENY);
            }
        }
    }

    public void deprive(User user) {
        Permission permission = new Permission();
        permission.setId(1);

        user.getPermissions().clear();
        user.getPermissions().add(permission);
        redisService.delete(user.getUsername());
    }
}
