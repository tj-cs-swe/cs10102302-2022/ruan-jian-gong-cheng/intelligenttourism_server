package team.softarrow.itourism_server.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import team.softarrow.itourism_server.entity.ServicePoint;
import team.softarrow.itourism_server.entity.Spot;
import team.softarrow.itourism_server.entity.TourRoute;
import team.softarrow.itourism_server.exception.DefinedException;
import team.softarrow.itourism_server.repository.TourRouteRepository;
import team.softarrow.itourism_server.response.ResponseCode;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
public class TourRouteService {
    private TourRouteRepository tourRouteRepository;
    private ServicePointService servicePointService;
    private PermissionService permissionService;
    private UserService userService;

    @Autowired
    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    @Autowired
    public void setCheckPermissionService(PermissionService permissionService) {
        this.permissionService = permissionService;
    }

    @Autowired
    public void setServicePointService(ServicePointService servicePointService) {
        this.servicePointService = servicePointService;
    }

    @Autowired
    public void setTourRouteRepository(TourRouteRepository tourRouteRepository) {
        this.tourRouteRepository = tourRouteRepository;
    }

    public void getPointList(TourRoute tourRoute) {
        if(tourRoute.getRoute() == null){
            throw new DefinedException(ResponseCode.NOT_MATCH,"浏览路线内容为空");
        }
        List<String> point_id = List.of(tourRoute.getRoute().split(";"));
        // 检查每个路径点是否存在
        for (String str_id : point_id) {
            int id = Integer.parseInt(str_id);
            ServicePoint servicePoint = servicePointService.getServicePointByID(id);
            if (!Objects.equals(servicePoint.getSpot().getId(), tourRoute.getSpot().getId())) {
                throw new DefinedException(ResponseCode.NOT_MATCH,"当前景区没有该景点");
            }
        }
    }

    public void checkTourRoute(TourRoute tourRoute) {
        if (tourRoute.getName() == null) {
            throw new DefinedException(ResponseCode.NOT_MATCH, "浏览路线名称为空");
        } else if (tourRoute.getIntroduce() == null) {
            throw new DefinedException(ResponseCode.NOT_MATCH, "浏览路线介绍为空");
        }
        getPointList(tourRoute);
    }

    /**
     * 获取游览路径概要
     * @param id 游览路径id
     * @return 游览luj
     */
    public TourRoute getTourRouteById(int id) {
        Optional<TourRoute> optional = tourRouteRepository.findById(id);
        if (optional.isEmpty()) {
            throw new DefinedException(ResponseCode.NOT_FOUND, "浏览路线不存在");
        }
        return optional.get();
    }

    /**
     * 添加TourRoute
     */
    public void addTourRoute(TourRoute tourRoute){
        Spot spot = new Spot();
        spot.setId(userService.getCurrent().getSpotId());
        tourRoute.setSpot(spot);
        checkTourRoute(tourRoute);

        tourRoute.setDeleted(false);
        tourRouteRepository.save(tourRoute);
    }

    /**
     * 编辑游览路径
     * @param tourRoute 游览路径
     */
    public void editTourRoute(TourRoute tourRoute) {
        if (tourRoute.getId() == null) {
            throw new DefinedException(ResponseCode.NOT_MATCH, "游览路线不存在");
        }
        TourRoute old_route = getTourRouteById(tourRoute.getId());
        permissionService.checkSpotPermission(old_route.getSpot().getId());
        if (tourRoute.getRoute() != null) {
            tourRoute.setSpot(old_route.getSpot());
            getPointList(tourRoute);
        }

        Optional.ofNullable(tourRoute.getName())
                .ifPresent(old_route::setName);
        Optional.ofNullable(tourRoute.getIntroduce())
                .ifPresent(old_route::setIntroduce);
        Optional.ofNullable(tourRoute.getRoute())
                .ifPresent(old_route::setRoute);
        tourRouteRepository.save(old_route);
    }

    /**
     * 删除TourRoute
     */
    public void deleteTourRoute(Integer tour_route_id){
        TourRoute tourRoute = getTourRouteById(tour_route_id);
        permissionService.checkSpotPermission(tourRoute.getSpot().getId());

        tourRouteRepository.deleteTourRoute(tour_route_id);
    }

    /**
     * 获取景区浏览路线列表
     */
    public List<TourRoute> getTourRouteList(Integer id, int page, int size) {
        Spot spot = new Spot();
        spot.setId(id);
        spot.setDeleted(false);

        Pageable pageable = PageRequest.of(page, size);
        Page<TourRoute> tourRoutes = tourRouteRepository.findBySpot(spot, pageable);
        List<TourRoute> tourRouteList = tourRoutes.getContent();
        for (TourRoute tourRoute : tourRouteList) {
            tourRoute.setSpot(null);
        }
        return tourRouteList;
    }

    /**
     * 获取浏览路线的详情
     * @param tid 游览路线的id
     * @return 详情
     */
    public List<ServicePoint> getRouteInfo(Integer tid) {
        TourRoute tourRoute = getTourRouteById(tid);
        List<ServicePoint> servicePoints = new ArrayList<>();
        List<String> point_id = List.of(tourRoute.getRoute().split(";"));

        // 根据标号获得景点信息
        for (String str_id : point_id) {
            int id = Integer.parseInt(str_id);
            ServicePoint servicePoint = servicePointService.getServicePointByID(id);
            servicePoint.setSpot(null);
            servicePoints.add(servicePoint);
        }

        return servicePoints;
    }
}
