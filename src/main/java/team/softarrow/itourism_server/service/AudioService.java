package team.softarrow.itourism_server.service;

import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import team.softarrow.itourism_server.entity.IntroAudio;
import team.softarrow.itourism_server.entity.ServicePoint;
import team.softarrow.itourism_server.exception.DefinedException;
import team.softarrow.itourism_server.repository.IntroAudioRepository;
import team.softarrow.itourism_server.repository.ServicePointRepository;
import team.softarrow.itourism_server.response.ResponseCode;

import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.List;
import java.util.Optional;

@Service
public class AudioService {
    private ServicePointRepository servicePointRepository;
    private IntroAudioRepository introAudioRepository;
    private PermissionService permissionService;

    @Value(value = "${resources_path}")
    private String resourcePath;

    @Autowired
    public void setCheckPermissionService(PermissionService permissionService) {
        this.permissionService = permissionService;
    }

    @Autowired
    public void setIntroAudioRepository(IntroAudioRepository introAudioRepository) {
        this.introAudioRepository = introAudioRepository;
    }

    @Autowired
    public void setServicePointRepository(ServicePointRepository servicePointRepository) {
        this.servicePointRepository = servicePointRepository;
    }

    public void checkIntroAudio(IntroAudio introAudio) {
        ServicePoint servicePoint = introAudio.getServicePoint();
        if (introAudio.getName() == null) {
            throw new DefinedException(ResponseCode.NOT_MATCH, "音频名称为空");
        } else if (introAudio.getIntroduce() == null) {
            throw new DefinedException(ResponseCode.NOT_MATCH, "音频介绍为空");
        } else if (servicePoint == null) {
            throw new DefinedException(ResponseCode.NOT_MATCH, "所属景点为空");
        } else if (servicePointRepository.findById(servicePoint.getId()).isEmpty()) {
            throw new DefinedException(ResponseCode.NOT_MATCH, "所属景点不存在");
        } else if (introAudio.getUrl() == null) {
            throw new DefinedException(ResponseCode.NOT_MATCH, "尚未上传音频文件");
        }
        File file = new File(resourcePath + "/audio/" + introAudio.getUrl());
        if (!file.exists()) {
            throw new DefinedException(ResponseCode.NOT_MATCH, "尚未上传音频文件");
        }
    }

    /**
     * 根据id获取音频文件
     * @param id 文件id
     * @return 音频
     */
    public IntroAudio getAudioById(int id) {
        Optional<IntroAudio> optional = introAudioRepository.findById(id);
        if (optional.isEmpty()) {
            throw new DefinedException(ResponseCode.NOT_FOUND, "找不到音频");
        }
        return optional.get();
    }

    /**
     * 添加音频
     * @param introAudio 音频
     */
    public void addIntroAudio(IntroAudio introAudio) {
        checkIntroAudio(introAudio);
        Optional<ServicePoint> optional = servicePointRepository.findById(introAudio.getServicePoint().getId());
        assert optional.isPresent();
        permissionService.checkSpotPermission(optional.get().getSpot().getId());

        introAudio.setDeleted(false);
        introAudioRepository.save(introAudio);
    }

    /**
     * 编辑音频
     * @param introAudio 音频
     */
    public void editIntroAudio(IntroAudio introAudio) {
        if (introAudio.getId() == null) {
            throw new DefinedException(ResponseCode.NOT_MATCH, "音频不存在");
        }

        // 取出要编辑的音频
        IntroAudio old_audio = getAudioById(introAudio.getId());
        permissionService.checkSpotPermission(old_audio.getServicePoint().getSpot().getId());

        // 如果修改了音频地址，需要检查是否已经存在
        if (introAudio.getUrl() != null) {
            File file = new File(resourcePath + "/audio/" + introAudio.getUrl());
            if (!file.exists()) {
                throw new DefinedException(ResponseCode.NOT_MATCH, "尚未上传音频文件");
            }
        }

        Optional.ofNullable(introAudio.getName())
                .ifPresent(old_audio::setName);
        Optional.ofNullable(introAudio.getIntroduce())
                .ifPresent(old_audio::setIntroduce);
        Optional.ofNullable(introAudio.getUrl())
                .ifPresent(old_audio::setUrl);

        introAudioRepository.save(old_audio);
    }

    /**
     * 删除音频
     * @param id 音频id
     */
    public void deleteIntroAudio(Integer id) {
        if (!introAudioRepository.existsById(id)) {
            throw new DefinedException(ResponseCode.NOT_FOUND, "音频不存在");
        }
        introAudioRepository.deleteIntroAudio(id);
    }

    /**
     * 获取景点的所有音频介绍
     * @param id 景点id
     * @return 音频介绍集合
     */
    public List<IntroAudio> getIntroAudioInServicePoint(int id, int page, int size) {
        ServicePoint servicePoint = new ServicePoint();
        servicePoint.setId(id);
        servicePoint.setDeleted(false);

        Pageable pageable = PageRequest.of(page, size);
        Page<IntroAudio> introAudios = introAudioRepository.findByServicePoint(servicePoint, pageable);
        List<IntroAudio> introAudioList = introAudios.getContent();
        for (IntroAudio introAudio : introAudioList) {
            introAudio.setServicePoint(null);
        }
        return introAudioList;
    }

    /**
     * 获取音频介绍详情
     * @param id 音频id
     * @return 音频介绍
     */
    public IntroAudio getIntroAudioInfo(int id) {
        IntroAudio introAudio = getAudioById(id);
        introAudio.getIntroduce();
        introAudio.setServicePoint(null);
        return introAudio;
    }

    /**
     * 根据音频id获取音频流
     */
    public void downloadAudio(int id, HttpServletResponse response) {
        IntroAudio introAudio = getAudioById(id);
        String fileUrl = introAudio.getUrl();

        OutputStream os = null;
        InputStream is= null;
        try {
            os = response.getOutputStream();
            response.reset();
            response.setContentType("application/x-download;charset=utf-8");
            response.setHeader("Content-Disposition", "attachment;filename="+ new String(fileUrl.getBytes("utf-8"),"ISO8859-1"));
            File f = new File(resourcePath + "/audio/" + fileUrl);
            is = new FileInputStream(f);
            IOUtils.copy(is, response.getOutputStream());
            response.getOutputStream().flush();
        } catch (IOException e) {
            throw new DefinedException(ResponseCode.OPERATE_BUSY,"下载失败");
        }
        finally {
            try {
                if (is != null) {
                    is.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                if (os != null) {
                    os.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
