package team.softarrow.itourism_server.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import team.softarrow.itourism_server.common.utils.PaymentUtils;
import team.softarrow.itourism_server.entity.Commodity;
import team.softarrow.itourism_server.entity.Order;
import team.softarrow.itourism_server.entity.Ticket;
import team.softarrow.itourism_server.exception.DefinedException;
import team.softarrow.itourism_server.repository.CommodityRepository;
import team.softarrow.itourism_server.repository.OrderRepository;
import team.softarrow.itourism_server.repository.SpotRepository;
import team.softarrow.itourism_server.repository.TicketRepository;
import team.softarrow.itourism_server.response.ResponseCode;

import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;

@Service
public class PayService {
    private OrderRepository orderRepository;
    private TicketRepository ticketRepository;
    private CommodityRepository commodityRepository;
    private SpotRepository spotRepository;
    private UserService userService;

    @Autowired
    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    @Autowired
    public void setSpotRepository(SpotRepository spotRepository) {
        this.spotRepository = spotRepository;
    }

    @Autowired
    public void setOrderRepository(OrderRepository orderRepository) {
        this.orderRepository = orderRepository;
    }

    @Autowired
    public void setTicketRepository(TicketRepository ticketRepository) {
        this.ticketRepository = ticketRepository;
    }

    @Autowired
    public void setCommodityRepository(CommodityRepository commodityRepository) {
        this.commodityRepository = commodityRepository;
    }

    /**
     * 根据ID获取订单
     * @param id 订单号
     * @return 订单
     */
    public Order getOrderByID(Long id) {
        Optional<Order> optional = orderRepository.findById(id);
        if (optional.isEmpty()) {
            throw new DefinedException(ResponseCode.NOT_FOUND, "订单不存在");
        }
        return optional.get();
    }

    /**
     * 更新订单状态
     * @param id 订单号
     * @param state 订单状态
     */
    public void updateOrder(Long id, Order.OrderState state) {
        Order order = getOrderByID(id);
        orderRepository.setState(id, state.ordinal());

        if (order.getOrderType() == Order.OrderType.Ticket) {
            Set<Ticket> tickets = order.getTickets();
            int spot_id = - 1;
            for (Ticket ticket : tickets) {
                if (spot_id == -1) spot_id = ticket.getSpot().getId();
                ticketRepository.setState(ticket.getId(), state.ordinal());
            }
            spotRepository.decreaseTicket(spot_id, tickets.size());
        } else if (order.getOrderType() == Order.OrderType.Commodity) {
            if (state.equals(Order.OrderState.Paid)) {
                Commodity commodity = order.getCommodity();
                commodityRepository.increaseSale(commodity.getId(), order.getAmount());
            }
        }
    }

    /**
     * 支付未付款的订单
     * @param id 订单id
     * @return 支付页面html
     */
    public Map<String, String> payOrder(Long id) throws Exception {
        Order order = getOrderByID(id);
        if (order.getOrderState() != Order.OrderState.Pending) {
            throw new DefinedException(ResponseCode.NOT_MATCH, "已经完成支付");
        }
        if (!Objects.equals(order.getUser().getId(), userService.getCurrent().getId())) {
            throw new DefinedException(ResponseCode.ACCESS_DENY);
        }

        String body = PaymentUtils.pay(order);
        return Map.of("html", body);
    }
}
