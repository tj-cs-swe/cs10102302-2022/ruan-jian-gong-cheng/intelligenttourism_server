package team.softarrow.itourism_server.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import team.softarrow.itourism_server.common.BaiduAI;
import team.softarrow.itourism_server.entity.Comment;
import team.softarrow.itourism_server.entity.Picture;
import team.softarrow.itourism_server.entity.ServicePoint;
import team.softarrow.itourism_server.entity.User;
import team.softarrow.itourism_server.exception.DefinedException;
import team.softarrow.itourism_server.repository.CommentRepository;
import team.softarrow.itourism_server.repository.ServicePointRepository;
import team.softarrow.itourism_server.response.ResponseCode;
import team.softarrow.itourism_server.security.VisitorDetail;

import java.io.File;
import java.util.*;

@Service
public class CommentService {
    private CommentRepository commentRepository;
    private ServicePointRepository servicePointRepository;
    private UserService userService;
    private PermissionService permissionService;
    private ServicePointService servicePointService;

    @Value(value = "${resources_path}")
    private String resourcePath;

    @Autowired
    public void setServicePointService(ServicePointService servicePointService) {
        this.servicePointService = servicePointService;
    }

    @Autowired
    public void setPermissionService(PermissionService permissionService) {
        this.permissionService = permissionService;
    }

    @Autowired
    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    @Autowired
    public void setServicePointRepository(ServicePointRepository servicePointRepository) {
        this.servicePointRepository = servicePointRepository;
    }

    @Autowired
    public void setCommentRepository(CommentRepository commentRepository) {
        this.commentRepository = commentRepository;
    }

    public void checkComment(Comment comment) {
        if (comment.getContain() == null) {
            throw new DefinedException(ResponseCode.NOT_MATCH, "评论内容为空");
        }
        if (comment.getServicePoint() == null || comment.getServicePoint().getId() == null) {
            throw new DefinedException(ResponseCode.NOT_MATCH, "归属景点为空");
        }
        if (!servicePointRepository.existsById(comment.getServicePoint().getId())) {
            throw new DefinedException(ResponseCode.NOT_MATCH, "归属景点不存在");
        }
        if (comment.getPictures() == null) return;
        for (Picture picture : comment.getPictures()) {
            File file = new File(resourcePath + "/image/" + picture.getPath());
            if (!file.exists()) {
                throw new DefinedException(ResponseCode.NOT_MATCH, "上传的图片不存在");
            }
        }
    }

    public List<Comment> getCommentList(ServicePoint servicePoint, int page, int size) {
        // 分页排序，置顶的放前面，然后再按时间排序
        List<Sort.Order> orders = new ArrayList<>();
        orders.add(new Sort.Order(Sort.Direction.DESC,"top"));
        orders.add(new Sort.Order(Sort.Direction.DESC,"time"));
        Pageable pageable = PageRequest.of(page, size, Sort.by(orders));
        Page<Comment> comments;

        VisitorDetail visitorDetail = userService.getCurrent();
        String permission = visitorDetail.getAuthorities() == null ? "ROLE_user" : visitorDetail.getAuthoritiesString();
        if (permission.contains("ROLE_user") || permission.contains("ROLE_seller")) {
            comments = commentRepository.findByServicePoint(servicePoint, pageable);
        } else if (permission.contains("ROLE_spot_admin")) {
            if (!Objects.equals(visitorDetail.getSpotId(), servicePoint.getSpot().getId())) {
                comments = commentRepository.findByServicePoint(servicePoint, pageable);
            } else {
                comments = commentRepository.findAllCommentByServicePoint(servicePoint, pageable);
            }
        } else {
            comments = commentRepository.findAllCommentByServicePoint(servicePoint, pageable);
        }

        return comments.getContent();
    }

    /**
     * 获取景点的所有评论
     * @param id 景点id
     * @return 评论集合
     */
    public List<Comment> getCommentInServicePoint(int id, int page, int size) {
        ServicePoint servicePoint = servicePointService.getServicePointByID(id);
        User me = new User();
        me.setId(userService.getCurrent().getId());

        List<Comment> commentList = getCommentList(servicePoint, page, size);
        for (Comment comment : commentList) {
            comment.setServicePoint(null);

            comment.setIfLike(comment.getUsers().contains(me));
            comment.setUsers(null);
            comment.setIfDislike(comment.getDownUsers().contains(me));
            comment.setDownUsers(null);
            User user = comment.getUser();
            user.setShop(null);
            user.setAdminSpot(null);
            user.setPassword(null);
        }
        return commentList;
    }

    /**
     * 返回评论详细
     * @param id 评论id
     * @return 评论
     */
    public Comment getCommentById(int id) {
        Optional<Comment> optional = commentRepository.findById(id);
        if (optional.isEmpty()) {
            throw new DefinedException(ResponseCode.NOT_MATCH, "评论不存在");
        }
        return optional.get();
    }

    /**
     * 添加评论
     * @param comment
     */
    public void addComment(Comment comment) {
        checkComment(comment);

        User me = new User();
        me.setId(userService.getCurrent().getId());

        comment.setUser(me);
        comment.setDeleted(false);
        comment.setTop(false);
        comment.setLikeNum(0);
        comment.setTime(new Date());
        comment.setVerified(BaiduAI.verify(comment.getContain()));
        commentRepository.save(comment);
    }

    /**
     * 评论互动之点赞
     */
    public boolean commentLike(Integer id) {
        Comment comment = getCommentById(id);

        User me = new User();
        me.setId(userService.getCurrent().getId());
        Set<User> usersLike = comment.getUsers();

        //如果已经点赞过
        if(usersLike.contains(me)){
            return false;
        }
        comment.setLikeNum(comment.getLikeNum() + 1);
        //记录点赞的用户
        usersLike.add(me);
        commentRepository.save(comment);
        return true;
    }

    /**
     * 评论互动之取消点赞
     */
    public boolean commentUnDoLike(Integer id) {
        //判断该用户是否点过赞
        User me = new User();
        me.setId(userService.getCurrent().getId());
        Comment comment = getCommentById(id);

        if(!comment.getUsers().contains(me)) return false;

        comment.setLikeNum(comment.getLikeNum() - 1);

        //在点赞列表中移除该用户
        comment.getUsers().remove(me);
        commentRepository.save(comment);
        return true;
    }

    /**
     * 踩评价
     * @param id 评价id
     */
    public void commentDisLike(Integer id) {
        User me = new User();
        me.setId(userService.getCurrent().getId());
        Comment comment = getCommentById(id);

        if (comment.getDownUsers().contains(me)) return;

        comment.getDownUsers().add(me);
        commentRepository.save(comment);
    }

    /**
     * 取消踩
     * @param id 评论id
     */
    public void commentUndoDisLike(Integer id) {
        User me = new User();
        me.setId(userService.getCurrent().getId());
        Comment comment = getCommentById(id);

        if (!comment.getDownUsers().contains(me)) return;

        comment.getDownUsers().remove(me);
        commentRepository.save(comment);
    }

    /**
     * 置顶评论
     * @param id 评论id
     */
    public void topComment(Integer id) {
        Comment comment = getCommentById(id);
        permissionService.checkSpotPermission(comment.getServicePoint().getSpot().getId());
        commentRepository.topComment(id);
    }

    /**
     * 删除评论
     * @param id 评论id
     */
    public void deleteComment(Integer id) {
        Comment comment = getCommentById(id);
        VisitorDetail visitorDetail = userService.getCurrent();
        String permission = visitorDetail.getAuthoritiesString();
        if ((permission.contains("ROLE_user") || permission.contains("ROLE_seller"))
                && !Objects.equals(visitorDetail.getId(), comment.getUser().getId())) {
            throw new DefinedException(ResponseCode.AUTHORITY_DENY, "没有权限");
        }
        permissionService.checkSpotPermission(comment.getServicePoint().getSpot().getId());

        commentRepository.deleteComment(id);
    }

    /**
     * 审核评论
     * @param id 评论Id
     */
    public void verifyComment(Integer id) {
        Comment comment = getCommentById(id);
        permissionService.checkSpotPermission(comment.getServicePoint().getSpot().getId());
        commentRepository.verifyComment(id);
    }
}
