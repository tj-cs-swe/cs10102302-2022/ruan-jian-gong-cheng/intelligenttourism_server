package team.softarrow.itourism_server.service;

import com.alibaba.fastjson.JSON;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import java.util.Map;
import java.util.Optional;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;

@Service
public class RedisService {
    private StringRedisTemplate stringRedisTemplate;

    @Autowired
    public void setStringRedisTemplate(StringRedisTemplate stringRedisTemplate) {
        this.stringRedisTemplate = stringRedisTemplate;
    }

    /**
     * 将键值对存入redis并设置过期时间
     * @param key 键
     * @param value 值
     * @param expire 过期时间
     * @param timeUnit 单位
     */
    public void set(String key, Object value, long expire, TimeUnit timeUnit) {
        stringRedisTemplate.opsForValue().set(key, (String) value, expire, timeUnit);
    }

    /**
     * 将键值对存入Redis
     * @param key 键
     * @param value 值
     */
    public void set(String key, Object value) {
        stringRedisTemplate.opsForValue().set(key, (String) value);
    }

    /**
     * 根据键获取值
     * @param key 键
     * @return 值
     */
    public String get(String key) {
        if (Boolean.TRUE.equals(stringRedisTemplate.hasKey(key)))
            return stringRedisTemplate.opsForValue().get(key);
        else
            return null;
    }

    /**
     * 获取键值的过期时间
     * @param key 键
     * @return 过期时间
     */
    public long getExpire(String key) {
        AtomicLong expire = new AtomicLong(-1);
        Optional.ofNullable(stringRedisTemplate.getExpire(key)).ifPresent(
                expire::set
        );
        return expire.get();
    }

    /**
     * 判断键是否存在
     * @param key 键
     * @return 存在与否
     */
    public boolean hasKey(String key) {
        return Boolean.TRUE.equals(stringRedisTemplate.hasKey(key));
    }

    /**
     * 将值反序列化为Map对象
     * @param value 值
     * @return Map对象
     */
    public Map<String, String> parseObject(String value) {
        return (Map<String, String>) JSON.parseObject(value, Map.class);
    }

    /**
     * 根据键来删除
     * @param key 键
     */
    public void delete(String key) {
        stringRedisTemplate.delete(key);
    }
}
