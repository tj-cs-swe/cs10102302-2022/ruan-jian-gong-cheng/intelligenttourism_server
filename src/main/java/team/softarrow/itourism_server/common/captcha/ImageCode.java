package team.softarrow.itourism_server.common.captcha;

import lombok.Data;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.Base64;
import java.util.UUID;

@Data
public class ImageCode {
    private UUID uuid;
    private String text;
    private String image;
    private LocalDateTime expireTime;

    /**
     * 将图片转为base64编码
     * @param image 图片
     * @throws IOException IO异常
     */
    public void setImage(BufferedImage image) throws IOException {
        try (ByteArrayOutputStream stream = new ByteArrayOutputStream()) {
            ImageIO.write(image, "jpeg", stream);
            byte[] bytes = stream.toByteArray();
            this.image = Base64.getEncoder().encodeToString(bytes);
        }

        this.image = "data:image/jpg;base64," + this.image;
    }

}
