package team.softarrow.itourism_server.common;

import com.alibaba.fastjson.JSONObject;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.web.client.RestTemplate;

import java.util.Map;

public class BaiduAI {
    private static String token = null;
    private static final String APP_KEY = "VrYn7kLZfVLsimNDOWVl2lj6";
    private static final String SECRET_KEY = "TiZib69vsGgj9BbcBbbZVv3CqHivF3ZO";

    public static void fetchToken() {
        if (token != null) return;

        String path = "https://aip.baidubce.com/oauth/2.0/token?";
        String getAccessTokenUrl = path
                // 1. grant_type为固定参数
                + "grant_type=client_credentials"
                // 2. 官网获取的 API Key
                + "&client_id=" + APP_KEY
                // 3. 官网获取的 Secret Key
                + "&client_secret=" + SECRET_KEY;
        RestTemplate restTemplate = new RestTemplate();

        // 设置header参数
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON_UTF8);

        // 设置jsonBody参数
        String body = "{ }";
        HttpEntity<String> entity = new HttpEntity<>(body, headers);

        String result = restTemplate.exchange(getAccessTokenUrl, HttpMethod.POST, entity, String.class).getBody();
        Map<String, String> json = JSONObject.parseObject(result, Map.class);

        token = json.get("access_token");
    }

    public static boolean verify(String text) {
        if (token == null) fetchToken();
        if (token == null) return false;

        String path = "https://aip.baidubce.com/rest/2.0/solution/v1/text_censor/v2/user_defined?";
        String url = path
                + "access_token=" + token;

        RestTemplate restTemplate = new RestTemplate();

        // 设置header参数
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

        // 设置jsonBody参数
        String body = "text=" + text;
        HttpEntity<String> entity = new HttpEntity<>(body, headers);

        String result = restTemplate.exchange(url, HttpMethod.POST, entity, String.class).getBody();
        Map<String, String> json = JSONObject.parseObject(result, Map.class);

        return json.get("conclusion").equals("合规");
    }
}
