package team.softarrow.itourism_server.common.utils;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import team.softarrow.itourism_server.common.SecurityConstants;
import team.softarrow.itourism_server.security.VisitorDetail;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Date;

/**
 * 生成令牌的工具类
 */
@Component
public class JwtUtil {

    public static String secret;
    @Value("${jwt.secret}")
    public void setSecret(String ymlSecret) {
        JwtUtil.secret = ymlSecret;
    }

    /**
     * 解析Token
     * @param token 待解析的Token
     * @return 用户的基本信息
     */
    public static VisitorDetail parseToken(String token) {
        Claims claims;
        try {
            claims = Jwts.parser()
                    .setSigningKey(secret)
                    .parseClaimsJws(token)
                    .getBody();
        } catch (Exception e) {
            return null;
        }

        // 如果Token已过期则不再解析
        if (claims.getExpiration().before(new Date())) {
            return null;
        }

        VisitorDetail visitorDetail = new VisitorDetail();
        visitorDetail.setUsername(claims.getSubject());
        visitorDetail.setId(claims.get("id", Integer.class));
        visitorDetail.setEmail(claims.get("email", String.class));
        visitorDetail.setAuthoritiesFromString(claims.get("perms", String.class));
        visitorDetail.setSpotId(claims.get("spot", Integer.class));
        visitorDetail.setShopId(claims.get("shop", Integer.class));

        return visitorDetail;
    }

    /**
     * 给用户设置一个Token
     * @param visitorDetail 用户信息
     * @return Token
     */
    public static String createToken(VisitorDetail visitorDetail) {
        LocalDateTime createdTime = LocalDateTime.now();
        LocalDateTime expirationTime = createdTime.plusDays(30);
        ZonedDateTime zonedDateTime = expirationTime.atZone(ZoneId.systemDefault());

        String token = Jwts.builder()
                    .setHeaderParam("type", "JWT")

                    .claim("id", visitorDetail.getId())
                    .claim("email", visitorDetail.getEmail())
                    .claim("perms", visitorDetail.getAuthoritiesString())
                    .claim("spot", visitorDetail.getSpotId())
                    .claim("shop", visitorDetail.getShopId())

                    .setSubject(visitorDetail.getUsername())
                    .setIssuedAt(new Date())
                    .signWith(SignatureAlgorithm.HS256, secret)
                    .setExpiration(Date.from(zonedDateTime.toInstant()))
                    .compact();
        return SecurityConstants.TOKEN_PREFIX + token;
    }
}
