package team.softarrow.itourism_server.common.utils;

import com.alipay.easysdk.factory.Factory;
import com.alipay.easysdk.payment.common.models.AlipayTradeRefundResponse;
import com.alipay.easysdk.payment.page.models.AlipayTradePagePayResponse;
import team.softarrow.itourism_server.entity.Order;
import team.softarrow.itourism_server.exception.DefinedException;
import team.softarrow.itourism_server.response.ResponseCode;

import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

public class PaymentUtils {

    /**
     * 向支付宝发起请求
     * @param order 订单
     * @return 支付宝的返回
     * @throws Exception 请求失败异常
     */
    public static String pay(Order order) throws Exception {
        String order_no = String.valueOf(order.getId());
        AlipayTradePagePayResponse response = Factory.Payment
                .Page()
                .optional("amount", order.getAmount())
                .pay(order.getOrderType().toString(),
                        order_no,
                        String.format("%.2f", order.getPrice()),
                        "https://www.baidu.com");
        return response.getBody();
    }

    /**
     * 退款
     * @param outTradeNo 订单号
     * @param amount 退款金额
     * @throws Exception 请求失败异常
     */
    public static void refund(String outTradeNo, String amount) throws Exception {
        AlipayTradeRefundResponse response = Factory.Payment
                .Common()
                .refund(outTradeNo, amount);
        if (!response.getMsg().equals("Success"))
            throw new DefinedException(ResponseCode.OPERATE_BUSY, "退款失败");
    }

    /**
     * 验签
     * @param requestParams 请求数据
     * @throws Exception 验签失败异常
     */
    public static void verifyNotify(Map<String,String[]> requestParams) throws Exception {
        Map<String,String> parameters = new HashMap<>();
        for (String name : requestParams.keySet()) {
            String[] values = requestParams.get(name);
            String valueStr = "";
            for (int i = 0; i < values.length; i++) {
                valueStr = (i == values.length - 1) ? valueStr + values[i]
                        : valueStr + values[i] + ",";
            }
            valueStr = new String(valueStr.getBytes(StandardCharsets.ISO_8859_1), StandardCharsets.UTF_8);
            parameters.put(name, valueStr);
        }
        Factory.Payment.Common().verifyNotify(parameters);
    }
}
