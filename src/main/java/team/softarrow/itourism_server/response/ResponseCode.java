package team.softarrow.itourism_server.response;

public enum ResponseCode {
    OK(200, "成功"),
    NOT_EXIST(401,"账号不存在"),
    PSW_ERROR(402,"用户名或密码错误"),
    NOT_MATCH(403,"格式错误"),
    NOT_FOUND(404,"未找到该资源"),
    AUTHORITY_DENY(405,"权限不足"),
    ACCESS_DENY(406,"获取用户认证失败"),
    CHECK_ERROR(407,"验证码错误"),
    TOKEN_ERROR(408,"Token错误或已过期"),
    EMAIL_TOO_OFTEN(409,"邮件请求过于频繁"),
    CONTENT_EMPTY(410,"提交内容为空"),
    CONDITION_NOT_MET(411,"不符合条件"),
    TOO_BIG(412,"提交内容过大"),

    SERVER_ERROR(500,"服务器内部错误"),
    REGISTER_FAIL(501,"注册失败"),
    OPERATE_BUSY(502,"操作失败"),
    SERVER_BUSY(503,"服务器正忙"),
    REPEAT(504,"资源重复");



    /**
     * 错误码
     */
    private final Integer code;
    /**
     * 错误信息
     */
    private final String message;

    ResponseCode(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }
}
