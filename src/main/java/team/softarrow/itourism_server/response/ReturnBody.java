package team.softarrow.itourism_server.response;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ReturnBody {
    //返回码
    private Integer status;
    //返回描述
    private String message;
    //返回数据
    private Object content;

    public static ReturnBody success(Object data) {
        ReturnBody result = new ReturnBody();
        result.setStatus(ResponseCode.OK.getCode());
        result.setMessage(ResponseCode.OK.getMessage());
        result.setContent(data);
        return result;
    }

    public static ReturnBody fail(ResponseCode code) {
        return fail(code, code.getMessage());
    }

    public static ReturnBody fail(ResponseCode code, String message) {
        ReturnBody result = new ReturnBody();
        result.setStatus(code.getCode());
        result.setMessage(message);
        return result;
    }

}
