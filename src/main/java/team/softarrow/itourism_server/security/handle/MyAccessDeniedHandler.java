package team.softarrow.itourism_server.security.handle;

import com.alibaba.fastjson.JSON;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.access.AccessDeniedHandler;
import org.springframework.stereotype.Component;
import team.softarrow.itourism_server.response.ResponseCode;
import team.softarrow.itourism_server.response.ReturnBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
public class MyAccessDeniedHandler implements AuthenticationEntryPoint, AccessDeniedHandler {

    /**
     * 认证失败
     * @param request
     * @param response
     * @param authException
     * @throws IOException
     */
    @Override
    public void commence(HttpServletRequest request,
                         HttpServletResponse response,
                         AuthenticationException authException) throws IOException {
        response.setContentType("text/json;charset=utf-8");
        response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
        ReturnBody result = ReturnBody.fail(ResponseCode.ACCESS_DENY);
        result.setContent(null);
        response.getWriter().write(JSON.toJSONString(result));
    }

    /**
     * 权限不足
     * @param request
     * @param response
     * @param accessDeniedException
     * @throws IOException
     */
    @Override
    public void handle(HttpServletRequest request,
                       HttpServletResponse response,
                       AccessDeniedException accessDeniedException) throws IOException {
        response.setContentType("text/json;charset=utf-8");
        response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());

        ReturnBody result = ReturnBody.fail(ResponseCode.AUTHORITY_DENY);
        result.setContent(null);
        response.getWriter().write(JSON.toJSONString(result));
    }
}
