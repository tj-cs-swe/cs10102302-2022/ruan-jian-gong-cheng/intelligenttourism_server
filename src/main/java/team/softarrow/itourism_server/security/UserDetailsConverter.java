package team.softarrow.itourism_server.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.transaction.annotation.Transactional;
import team.softarrow.itourism_server.entity.User;
import team.softarrow.itourism_server.repository.UserRepository;

import java.util.Optional;

public class UserDetailsConverter implements UserDetailsService {

    private UserRepository userRepository;

    @Autowired
    public void setVisitorRepository(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    @Transactional
    public UserDetails loadUserByUsername(String username) {
        Optional<User> optional = userRepository.findVisitorByUsername(username);
        if (optional.isPresent()) {
            return VisitorDetail.build(optional.get());
        }
        throw new UsernameNotFoundException("用户不存在");
    }
}
