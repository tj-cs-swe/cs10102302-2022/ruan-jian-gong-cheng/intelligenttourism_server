package team.softarrow.itourism_server.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.authentication.configuration.EnableGlobalAuthentication;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import team.softarrow.itourism_server.security.filter.LoginFilter;
import team.softarrow.itourism_server.security.filter.TokenFilter;
import team.softarrow.itourism_server.security.handle.MyAccessDeniedHandler;

@Configuration
@EnableWebSecurity
@EnableGlobalAuthentication
public class WebSecurityConfiguration extends WebSecurityConfigurerAdapter {

    private MyAccessDeniedHandler myAccessDeniedHandler;

    @Autowired
    public void setMyAccessDeniedHandler(MyAccessDeniedHandler myAccessDeniedHandler) {
        this.myAccessDeniedHandler = myAccessDeniedHandler;
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .authorizeRequests()
                    .antMatchers("/auth/**").permitAll()
                    .antMatchers("/pay/result").permitAll()
                    .antMatchers("/spot/{id:\\d+}").permitAll()
                    .antMatchers("/spot/list/**").permitAll()
                    .antMatchers("/shop/scene/**", "/shop/store/**", "/shop/item/{id:\\d+}", "/shop/{id:\\d+}").permitAll()
                    .antMatchers("/travel/comment/{id:\\d+}/**").permitAll()
                    .antMatchers("/travel/service_point/**").permitAll()
                    .antMatchers("/travel/tour_route/**").permitAll()
                    .antMatchers("/travel/intro_audio/list/**").permitAll()
                    .antMatchers("/travel/intro_audio/{id:\\d+}").permitAll()
                    .antMatchers("/admin/**").hasRole("admin")
                    .antMatchers("/spot_admin/**").hasAnyRole("admin", "spot_admin")
                    .antMatchers("/shop/goods/**").hasAnyRole("admin", "spot_admin", "seller")
                    .antMatchers("/shop/order/**").hasAnyRole("admin", "spot_admin", "seller")
                    .antMatchers("/shop/edit").hasAnyRole("admin", "spot_admin", "seller")
                    .anyRequest().authenticated()
                .and()
                    .sessionManagement()
                    .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                    .exceptionHandling()
                    .authenticationEntryPoint(myAccessDeniedHandler)
                    .accessDeniedHandler(myAccessDeniedHandler)
                .and().csrf().disable();
        http
                .addFilterBefore(new LoginFilter("/auth/login", authenticationManager()), UsernamePasswordAuthenticationFilter.class)
                .addFilterBefore(new TokenFilter(authenticationManager()), UsernamePasswordAuthenticationFilter.class);
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService());
        auth.inMemoryAuthentication().passwordEncoder(passwordEncoder());
    }

    @Override
    public void configure(WebSecurity web) {
        web.ignoring().antMatchers("/static/**");
    }

    @Bean
    public BCryptPasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    public UserDetailsConverter userDetailsService() {
        return new UserDetailsConverter();
    }
}
