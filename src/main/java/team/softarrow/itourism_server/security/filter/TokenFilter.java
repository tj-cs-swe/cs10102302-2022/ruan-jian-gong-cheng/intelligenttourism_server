package team.softarrow.itourism_server.security.filter;

import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;
import team.softarrow.itourism_server.common.SecurityConstants;
import team.softarrow.itourism_server.common.utils.JwtUtil;
import team.softarrow.itourism_server.common.utils.SpringContextUtils;
import team.softarrow.itourism_server.security.VisitorDetail;
import team.softarrow.itourism_server.service.RedisService;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Token过滤器
 * <br>
 * 检查请求中是否带有合法的Token
 */
public class TokenFilter extends BasicAuthenticationFilter {

    public TokenFilter(AuthenticationManager authenticationManager) {
        super(authenticationManager);
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request,
                                    HttpServletResponse response,
                                    FilterChain chain) throws IOException, ServletException {

        String token = request.getHeader(SecurityConstants.TOKEN_HEADER);
        // 如果没有Token那就进入下一个过滤器
        if (token == null || !token.startsWith(SecurityConstants.TOKEN_PREFIX)) {
            chain.doFilter(request, response);
            return;
        }

        token = token.replace(SecurityConstants.TOKEN_PREFIX, "");
        VisitorDetail visitorDetail = JwtUtil.parseToken(token);
        RedisService redisService = SpringContextUtils.getBean(RedisService.class);
        if (visitorDetail == null || !redisService.hasKey(visitorDetail.getUsername())) {
            chain.doFilter(request, response);
            return;
        }
        String store_token = redisService.get(visitorDetail.getUsername());
        if (!store_token.equals(SecurityConstants.TOKEN_PREFIX + token)) {
            chain.doFilter(request, response);
            return;
        }

        UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(visitorDetail, null, visitorDetail.getAuthorities());
        SecurityContextHolder.getContext().setAuthentication(authentication);
        chain.doFilter(request, response);
    }
}
