package team.softarrow.itourism_server.security.filter;

import com.alibaba.fastjson.JSON;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import team.softarrow.itourism_server.common.utils.JwtUtil;
import team.softarrow.itourism_server.common.utils.SpringContextUtils;
import team.softarrow.itourism_server.exception.CheckException;
import team.softarrow.itourism_server.response.ResponseCode;
import team.softarrow.itourism_server.response.ReturnBody;
import team.softarrow.itourism_server.security.VisitorDetail;
import team.softarrow.itourism_server.service.RedisService;

import javax.servlet.FilterChain;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;

import static team.softarrow.itourism_server.response.ResponseCode.CHECK_ERROR;

/**
 * 重写登录时的过滤器
 * <br>
 * 登录成功后会返回一个Token
 */
public class LoginFilter extends AbstractAuthenticationProcessingFilter {

    public LoginFilter(String defaultFilterProcessesUrl,
                       AuthenticationManager authenticationManager) {
        super(new AntPathRequestMatcher(defaultFilterProcessesUrl));
        setAuthenticationManager(authenticationManager);
    }

    @Override
    public Authentication attemptAuthentication(HttpServletRequest request,
                                                HttpServletResponse response) throws AuthenticationException, IOException {
        VisitorDetail visitorDetail = new ObjectMapper().readValue(request.getInputStream(), VisitorDetail.class);

        // 从redis中获取验证码进行校验
        RedisService redisService = SpringContextUtils.getBean(RedisService.class);
        String captcha = redisService.get(visitorDetail.getKey());
        redisService.delete(visitorDetail.getKey());
        if (captcha == null || !captcha.equals(visitorDetail.getCaptcha())) {
            throw new CheckException(CHECK_ERROR);
        }

        // 校验密码
        UsernamePasswordAuthenticationToken authRequest;
        authRequest = new UsernamePasswordAuthenticationToken(visitorDetail.getUsername(), visitorDetail.getPassword());
        authRequest.setDetails(authenticationDetailsSource.buildDetails(request));

        return this.getAuthenticationManager().authenticate(authRequest);
    }

    @Override
    protected void successfulAuthentication(HttpServletRequest request,
                                            HttpServletResponse response,
                                            FilterChain chain,
                                            Authentication authResult) throws IOException {
        response.setContentType("text/json;charset=utf-8");
        VisitorDetail visitorDetail = (VisitorDetail) authResult.getPrincipal();
        // 判断是否被封禁了
        if (visitorDetail.getAuthoritiesString().contains("ROLE_ban")) {
            response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
            ReturnBody result = ReturnBody.fail(ResponseCode.ACCESS_DENY, "已被封禁");
            response.getWriter().write(JSON.toJSONString(result));
            return;
        }
        String token = JwtUtil.createToken(visitorDetail);

        // 将Token缓存进redis
        RedisService redisService = SpringContextUtils.getBean(RedisService.class);
        redisService.set(visitorDetail.getUsername(), token);

        // 回传构建好的token
        ReturnBody result = ReturnBody.success(Map.of("token", token));
        response.getWriter().write(JSON.toJSONString(result));
    }

    @Override
    protected void unsuccessfulAuthentication(HttpServletRequest request,
                                              HttpServletResponse response,
                                              AuthenticationException failed) throws IOException {
        response.setContentType("text/json;charset=utf-8");
        response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());

        ReturnBody result;
        if (failed instanceof CheckException) {
            result = ReturnBody.fail(CHECK_ERROR);
        } else {
            result = ReturnBody.fail(ResponseCode.PSW_ERROR);
        }
        result.setContent(null);
        response.getWriter().write(JSON.toJSONString(result));
    }
}
