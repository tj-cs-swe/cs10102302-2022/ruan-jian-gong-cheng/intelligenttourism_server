package team.softarrow.itourism_server.security;

import lombok.Data;
import org.hibernate.validator.constraints.Length;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import team.softarrow.itourism_server.entity.Permission;
import team.softarrow.itourism_server.entity.User;

import javax.validation.constraints.Email;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Data
public class VisitorDetail implements UserDetails {
    private Integer id;
    private String key;
    @Length(max = 32, message = "用户名长度超限")
    private String username;
    @Length(min = 8, max = 16, message = "密码长度应在8~16间")
    private String password;
    @Length(min = 8, max = 16, message = "密码长度应在8~16间")
    private String checkPassword;
    @Email(message = "邮箱格式错误")
    private String email;
    private String captcha;

    private Integer shopId;
    private Integer spotId;

    private boolean accountNonExpired = true;
    private boolean accountNonLocked = true;
    private boolean credentialsNonExpired = true;
    private boolean enabled = true;
    private List<GrantedAuthority> authorities;

    /**
     * 从完整的用户对象中抽取重要的信息组成一个简单用户信息对象<br/>
     * 再加上前端发回的一些额外信息
     * @param user 完整的用户对象
     * @return 简单用户信息对象
     */
    public static VisitorDetail build(User user) {
        List<GrantedAuthority> authorities = new ArrayList<>();
        for (Permission perm : user.getPermissions()) {
            authorities.add(new SimpleGrantedAuthority(perm.getName()));
        }
        VisitorDetail detail = new VisitorDetail();
        detail.setId(user.getId());
        detail.setUsername(user.getUsername());
        detail.setPassword(user.getPassword());
        detail.setEmail(user.getEmail());
        detail.setAuthorities(authorities);
        Optional.ofNullable(user.getShop()).ifPresent(shop -> detail.setShopId(shop.getId()));
        Optional.ofNullable(user.getAdminSpot()).ifPresent(spot -> detail.setSpotId(spot.getId()));
        return detail;
    }

    /**
     * 将权限转换为字符串
     * @return 字符串形式的权限列表
     */
    public String getAuthoritiesString() {
        StringBuilder builder = new StringBuilder();
        for (GrantedAuthority authority : authorities) {
            builder.append(authority.getAuthority());
            builder.append(",");
        }
        return builder.toString();
    }

    /**
     * 转换字符串形式的权限列表
     * @param authorities 字符串形式的权限列表
     */
    public void setAuthoritiesFromString(String authorities) {
        this.authorities = AuthorityUtils.commaSeparatedStringToAuthorityList(authorities);
    }
}
